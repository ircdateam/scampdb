﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;
using Newtonsoft.Json;
using ScampsDB.Extensions.Json;

namespace ScampsDB.Query
{
    public static class DataDictionary
    {
        public static void getSCAMPs(System.IO.TextWriter wt)
        {
            var ErrorList = new string[] { 
                "Could not retrieve SCAMP list"
            };

            var t = SCAMPstore.Get();

            var js = new JsonTextWriter(wt);

            try
            {
                js.WriteStartArray();

                t.SCAMPs.ToList().ForEach(sc =>
                    js.WrtStartObject()
                        .WrtPropertyValue("scamp_id", sc.SCAMP_ID)
                        .WrtPropertyValue("scamp_name", sc.SCAMP_NAME)
                        .WrtPropertyValue("display_name", sc.DISPLAY_NAME)
                        .WrtPropertyValue("visible", sc.VISIBLE)
                    .WrtEndObject()
                );
                js.WriteEndArray();
            }
            catch (Exception e)
            {
                if (js.WriteState == Newtonsoft.Json.WriteState.Property)
                    js.WriteNull();
                if (js.WriteState == Newtonsoft.Json.WriteState.Object)
                    js.WriteEndObject();
                if (js.WriteState == Newtonsoft.Json.WriteState.Array)
                    js.WriteEndArray();

                throw new SCAMPException(ErrorList[0]);
            }
        }

        public static void getSCAMPforms(string scamp_id, System.IO.TextWriter wt)
        {

            var ErrorList = new string[] { 
                "Could not read form entries for SCAMP"
            };

            var t = SCAMPstore.Get();

            var js = new JsonTextWriter(wt);


            try
            {
                js.WriteStartArray();

                int id = 0;
                id = int.Parse(scamp_id);

                t.ADMIN_ENCOUNTERS.Where(en => en.SCAMP.SCAMP_ID == id).ToList().ForEach(en =>
                    js.WrtStartObject()
                        .WrtPropertyValue("scamp_id", en.SCAMP_ID)
                        .WrtPropertyValue("encounter_id", en.ENCOUNTER_ID)
                        .WrtPropertyValue("encounter_name", en.ENCOUNTER_NAME)
                        .WrtPropertyValue("form_name", en.FORM_NAME)
                        .WrtPropertyValue("web_form_approved", en.WEB_FORM_APPROVED)
                        .WrtPropertyValue("visible_flag", en.VISIBLE_FLAG)
                        .WrtPropertyValue("seq_num", en.SEQ_NUM)
                    .WrtEndObject()
                );
                js.WriteEndArray();
            }
            catch (Exception e)
            {
                if (js.WriteState == Newtonsoft.Json.WriteState.Property)
                    js.WriteNull();
                if (js.WriteState == Newtonsoft.Json.WriteState.Object)
                    js.WriteEndObject();
                if (js.WriteState == Newtonsoft.Json.WriteState.Array)
                    js.WriteEndArray();

                throw new SCAMPException(ErrorList[0]);
            }
        }

        public static void getFormElements(string form_name, System.IO.TextWriter wt)
        {
            var ErrorList = new string[]{
                                   "Could not read elements for form"
                               };

            var t = SCAMPstore.Get();

            var js = new JsonTextWriter(wt);
            try
            {
                js.WriteStartArray();
                t.ADMIN_FORM_FIELDS.Where(ff => ff.FORM_NAME.ToLower() == form_name.ToLower()).ToList().ForEach(ff =>
                    js.WrtStartObject()
                        .WrtPropertyValue("element_cd", ff.ELEMENT_CD)
                        .WrtPropertyValue("description", ff.DESCR_TXT)
                        .WrtPropertyValue("control", ff.CONTROL_TYPE)
                        .WrtPropertyValue("multiline", ff.TEXTMODE == "MULTILINE")
                        .WrtPropertyValue("required", ff.REQUIRED_FLAG == "1")
                        .WrtPropertyValue("visible", ff.VISIBLE_FLAG == "1")
                    .WrtEndObject()
                );
                js.WriteEndArray();
            }
            catch (Exception e)
            {
                if (js.WriteState == Newtonsoft.Json.WriteState.Property)
                    js.WriteNull();
                if (js.WriteState == Newtonsoft.Json.WriteState.Object)
                    js.WriteEndObject();
                if (js.WriteState == Newtonsoft.Json.WriteState.Array)
                    js.WriteEndArray();

                throw new SCAMPException(ErrorList[0]);
            }
        }

        public static void getElementList(string list_cd, System.IO.TextWriter wt)
        {
            var ErrorList = new string[]{
                                   "Could not read list information for element"
                               };

            var t = SCAMPstore.Get();

            var js = new JsonTextWriter(wt);

            try
            {
                js.WrtStartArray();

                t.ADMIN_ELEMENT_LIST.Where(ls => ls.LIST_CD.ToLower() == list_cd.ToLower()).ToList().ForEach(ls =>
                    js.WrtStartObject()
                        .WrtPropertyValue("id", ls.ID)
                        .WrtPropertyValue("description_txt", ls.DESCRIPTION_TXT)
                        .WrtPropertyValue("value_txt", ls.VALUE_TXT)
                        .WrtPropertyValue("seq_num", ls.SEQ_NUM)
                        .WrtPropertyValue("visible_abbr", ls.VISIBLE_ABBR)
                    .WrtEndObject()
                );
                js.WriteEndArray();
            }
            catch (Exception e)
            {
                if (js.WriteState == Newtonsoft.Json.WriteState.Property)
                    js.WriteNull();
                if (js.WriteState == Newtonsoft.Json.WriteState.Object)
                    js.WriteEndObject();
                if (js.WriteState == Newtonsoft.Json.WriteState.Array)
                    js.WriteEndArray();

                throw new SCAMPException(ErrorList[0]);
            }
        }

        public static void getElementInfo(string element_cd, System.IO.TextWriter wt)
        {
            var ErrorList = new string[]{
                                   "Could not read info for element"
                               };

            var t = SCAMPstore.Get();

            var js = new JsonTextWriter(wt);

            try
            {
                js.WrtStartArray();
                js.WriteStartObject();
                t.ADMIN_ELEMENT.Where(el => el.ELEMENT_CD.ToLower() == element_cd.ToLower()).ToList().ForEach(el =>
                        js.WrtPropertyValue("element_cd", el.ELEMENT_CD)
                        .WrtPropertyValue("description", el.DESCRIPTION_TXT)
                        .WrtPropertyValue("list_cd", el.LIST_CD)
                );
                js.WriteEndObject();
                js.WriteEndArray();
            }
            catch (Exception e)
            {
                if (js.WriteState == Newtonsoft.Json.WriteState.Property)
                    js.WriteNull();
                if (js.WriteState == Newtonsoft.Json.WriteState.Object)
                    js.WriteEndObject();
                if (js.WriteState == Newtonsoft.Json.WriteState.Array)
                    js.WriteEndArray();

                throw new SCAMPException(ErrorList[0]);
            }
        }

    }
}
