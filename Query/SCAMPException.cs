﻿using System;
namespace ScampsDB.Query
{
    public class SCAMPException : Exception
    {
        public SCAMPException(string message)
            : base(message)
        {
        }
    }

}
