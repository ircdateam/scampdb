﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;

namespace ScampsDB.Query
{
    public static class FormFields
    {
        /*
         * returns an enumeration of objects, each object containing 3 properties: name, val cat
         * cat is short for category, determines the name,value collection
         * the name property provides a 'human-readable' description of the value, and val typically corresponds to the unique id for name
         * e.g. cat (category) == 0 corresponds to SCAMP name (name) and SCAMP id (value)
         * 
         * category enumerations:
         * 0 - SCAMP
         * 1 - Data Coordinators
         * 2 - Providers
         * 3 - Outpatient locations
         * 4 - Inpatient locations
         * 5 - Event locations
         * 6 - Event Types
         * 7 - Encounter statuses
         * 8 - Encounter names corresponding to form names 
         * 9 - SCAMP ids corresponding to form names
         */
        public static IEnumerable<SelectOptionModel> getOptList(string[] categories)
        {
            if (categories == null || categories.Length < 1)
                yield return null;
            //yield return new SelectOptionModel[0];

            const string _getOptQuery = "select name, val, cat from ENCOUNTER_OPTS_VIEW where cat in ({0}) order by cat desc";
            const string _getOrderedEnc = "select coalesce(encounter_name,encounter_name_short) as name, TO_CHAR(encounter_id) as val, 8 as cat from admin_encounters where VISIBLE_FLAG = 'Y' AND WEB_FORM_APPROVED = 'Y' order by SCAMP_ID, SEQ_NUM";

            //union select on option view doesn't give encounters and their corresponding 
            //var whereClause = categories.Aggregate((a,b) => a + ','+b);
            var whereClause = categories.Where(str => !str.Equals("8")).Aggregate((a, b) => a + ',' + b);

            var ctx = SCAMPstore.Get();
            foreach (var opt in ctx.ExecuteStoreQuery<SelectOptionModel>(string.Format(_getOptQuery, whereClause)))
                yield return opt;

            if (categories.Contains("8"))
            {
                foreach (var opt in ctx.ExecuteStoreQuery<SelectOptionModel>(_getOrderedEnc))
                    yield return opt;
            }

        }
    }
}
