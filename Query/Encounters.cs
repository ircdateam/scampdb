﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;
using ScampsDB.SearchModels;
using System.Data.Objects;
using ScampsDB.Extensions.String;

namespace ScampsDB.Query
{
    public static class Encounters
    {
        private const string datemask = "MM/dd/yyyy";
        private const string datetimemask = "g";

        public static void linkEncounters(string source_id, string[] target_ids)
        {
            var ErrorList = new string[] {
                "null or empty encounter id",
                "invalid encounter id",
                "one or more invalid target ids",
                "encounter doesn't exist",
                "one or more target ids do not exist",
            };

            //can't link empty id
            if (string.IsNullOrEmpty(source_id))
                throw new SCAMPException(ErrorList[0]);

            long appt_id = 0;

            if (!long.TryParse(source_id, out appt_id))
                throw new SCAMPException(ErrorList[1]);

            var targets = new long[target_ids.Length];

            for (var i = 0; i < target_ids.Length; i++)
            {
                long tmp = 0;
                if (!long.TryParse(target_ids[i], out tmp))
                    throw new SCAMPException(ErrorList[2]);
                targets[i] = tmp;
            }

            var t = SCAMPstore.Get();

            PT_ENCOUNTERS source_en = null;
            if ((source_en = t.PT_ENCOUNTERS.FirstOrDefault(en => en.APPT_ID == appt_id)) == null)
                throw new SCAMPException(ErrorList[3]);

            //delete old links
            const string _delEncLink = "delete from encounter_link where source_id = :01";
            t.ExecuteStoreCommand(_delEncLink, source_id);

            if (targets.Length == 0)
                return;

            if (t.PT_ENCOUNTERS.Where(en => en.MRN.ToUpper() == source_en.MRN.ToUpper()).Count(en => targets.Contains(en.APPT_ID)) != targets.Length)
                throw new SCAMPException(ErrorList[4]);

            //input verified, insert new links

            var newLinks = targets.Select(tl => tl.ToString()).Aggregate((l1, l2) => l1 + "," + l2);

            const string _insertEncLink = "insert into encounter_link (source_id, source_cd, target_id, target_cd) select e1.appt_id as source_id, e1.encounter_cd as source_cd, e2.appt_id as target_id, e2.encounter_cd as target_cd from pt_encounters e1 join pt_encounters e2 on e1.mrn = e2.mrn and e1.appt_id != e2.appt_id where e1.appt_id = {0} and e2.appt_id in ({1})";

            t.ExecuteStoreCommand(string.Format(_insertEncLink, source_id, newLinks));

        }

        public static void createSubUnitEncounter(EncounterViewModel en, out string new_sub_id)
        {
            var ErrorList = new string[] {
                "Unable to retrieve root appointment id for new subunit",
                "Patient related to encounter does not exist",
                "Encounter date occurs before patient birth",
                "Invalid SCAMP specified for encounter",
                "Invalid form specified for encounter",
                "Invalid location"
            };

            new_sub_id = string.Empty;
            var rootHAR = string.Empty;
            var t = SCAMPstore.Get();

            try
            {
                var baseEncounter = t.PT_ENCOUNTERS.FirstOrDefault(pt => pt.APPT_ID == en.appt_id);

                if (baseEncounter == null)
                    throw new SCAMPException(ErrorList[0]);
                rootHAR = baseEncounter.HAR;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Unable to retrieve root appointment id for new subunit: " + e.ToString());
                throw new SCAMPException(ErrorList[0]);
            }


            ADMIN_PROVIDER provider = null;
            ADMIN_ENCOUNTERS encountertype = null;

            //check if given patient exists
            var enpt = t.PT_MASTER.FirstOrDefault(pt => pt.MRN.ToUpper() == en.mrn.ToUpper());

            if (enpt == null)
                throw new SCAMPException(ErrorList[1]);

            if (enpt.DOB.HasValue && en.encounter_date.HasValue && enpt.DOB.Value > en.encounter_date.Value)
                throw new SCAMPException(ErrorList[2]);



            //check if given provider exists
            if (en.scamps_prov_id.HasValue)
                provider = t.ADMIN_PROVIDER.FirstOrDefault(pr => pr.SCAMPS_PROV_ID == en.scamps_prov_id.Value);

            //if inpatient encounter, need valid SCAMP ID value ...
            if (en.encounter_cd == "I" && (!en.scamp_id.HasValue && !t.SCAMPs.Any(sc => sc.SCAMP_ID == en.scamp_id.Value)))
                throw new SCAMPException(ErrorList[3]);

            //validate scamp && encounter type
            if (en.scamp_id.HasValue && en.encounter_type.HasValue)
                encountertype = t.ADMIN_ENCOUNTERS.FirstOrDefault(enc => enc.ENCOUNTER_ID == en.encounter_type.Value && enc.SCAMP_ID == en.scamp_id.Value);

            if (encountertype == null)
                throw new SCAMPException(ErrorList[4]);

            //check for sibling subunit encounters and assign an appropriate seq_num to current encounter
            short subseq = 1;
            var maxsubseq = t.VISITS_INPT_SUBUNIT.Where(vs => vs.HAR == rootHAR).Max(vs => vs.SEQ_VAL);

            if (maxsubseq.HasValue)
                subseq = (short)(maxsubseq.Value + 1);

            //lookup location
            var locfilter = "LOCTN_INPT";
            int? loctn_id = null;
            var clinic_name = string.Empty;

            if (!string.IsNullOrEmpty(en.loctn_abbr))
            {
                var loctn_lookup = t.ADMIN_LOOKUP.FirstOrDefault(loc => loc.LOOKUP_CD == locfilter && loc.VALUE_TXT == en.loctn_abbr);
                if (loctn_lookup == null)
                    throw new SCAMPException(ErrorList[5]);
                clinic_name = en.loctn_abbr;
                loctn_id = loctn_lookup.ID;
            }

            var sub = new VISITS_INPT_SUBUNIT()
            {
                HAR = rootHAR,
                CLINIC_NAME = clinic_name,
                DATE_DT = en.encounter_date,
                ENCOUNTER_TYPE = encountertype.ENCOUNTER_ID,
                ENCOUNTER_STATUS = en.encounter_status.ToString(),
                SCAMP_ID = en.scamp_id.Value,
                SEQ_VAL = subseq,
                ENCOUNTER_NOTES = en.encounter_notes,
                DATA_ENTERED = "N",
                DISPLAY_TXT = string.Concat("Day-", subseq),
                CLINIC_NO = loctn_id
            };

            if (en.encounter_user.HasValue && t.ADMIN_PROVIDER.Any(pr => pr.SCAMPS_PROV_ID == en.encounter_user))
                sub.ENCOUNTER_USER = en.encounter_user;

            if (provider != null)
            {
                sub.ATTENDING = provider.FULL_NAME;
                sub.SCAMPS_PROV_ID = provider.SCAMPS_PROV_ID;
            }

            try
            {
                t.VISITS_INPT_SUBUNIT.AddObject(sub);
                t.SaveChanges();

                t.Refresh(RefreshMode.StoreWins, sub);

                if (sub.ID == 0)
                    sub.ID = t.VISITS_INPT_SUBUNIT.Where(vs => vs.HAR == rootHAR).Max(vs => vs.ID);

                new_sub_id = sub.ID.ToString();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Unable to save new SubUnit Encounter: " + e.ToString());
            }
        }

        /* date, visit type, location, provider, scamp, encounter_type
         */
        public static void createEncounter(EncounterViewModel en, out string new_appt_id)
        {
            new_appt_id = string.Empty;

            const string _queryApptId = "select to_char(pt_encounters_appt_id_seq.nextval) as key, to_char(-1) as value from dual";
            const string _queryApptIdHar = "select to_char(pt_encounters_appt_id_seq.nextval) as key, to_char(pt_encounters_har_seq.nextval) as value from dual";

            var ErrorList = new string[] { 
                "no data",
                "MRN missing",
                "invalid encounter code",
                "no patient exists for given MRN",
                "Invalid SCAMP id specified",
                "Invalid encounter type specified",
                "could not retrieve generated HAR for encounter",
                "encounter date occurs before patient birth",
                "invalid location",
                "HAR already exists for another inpatient encounter",//9
                "HAR exceeds maximum length",
                "Invalid HAR format",
                "Invalid provider specified"//12
            };

            if (en == null)
                throw new SCAMPException(ErrorList[0]);

            if (string.IsNullOrEmpty(en.mrn))
                throw new SCAMPException(ErrorList[1]);

            if (string.IsNullOrEmpty(en.encounter_cd) || en.encounter_cd.Length != 1 || !"OIE".Contains(en.encounter_cd))
                throw new SCAMPException(ErrorList[2]);

            ADMIN_PROVIDER provider = null;
            ADMIN_ENCOUNTERS encountertype = null;
            long apptid = 0;
            string HAR = en.har;

            //lots of queries create a new appointment... 
            var t = SCAMPstore.Get();

            //check HAR if manually set
            if (!string.IsNullOrEmpty(HAR))
            {
                if (HAR.Length > 10)
                    throw new SCAMPException(ErrorList[10]);
                //HAR sequence generated is XX200000 to XX99999999 ... prevent collisions
                //note that XX concatenated with a padded number is OK, as this won't cause a collision
                if (HAR.StartsWith("XX", StringComparison.OrdinalIgnoreCase) && !HAR.Substring(2).StartsWith("0"))
                {
                    int harnumber = 0;
                    //if har is not padded with a zero but a parsable number greater than 200000 
                    //(PT_ENCOUNTERS_HAR_SEQ is defined to start at 200001), then it is a potential collision
                    if (int.TryParse(HAR.Substring(2), out harnumber) && harnumber > 200000)
                        throw new SCAMPException(ErrorList[11]);
                }
            }

            //check if given patient exists
            var enpt = t.PT_MASTER.FirstOrDefault(pt => pt.MRN.ToUpper() == en.mrn.ToUpper());
            if (enpt == null)
                throw new SCAMPException(ErrorList[3]);

            //if encounter date occurs before patient dob, encounter date is invalid
            if (en.encounter_date.HasValue && enpt.DOB.HasValue && enpt.DOB.Value > en.encounter_date.Value)
                throw new SCAMPException(ErrorList[7]);

            //check if given provider exists
            if (en.scamps_prov_id.HasValue){
                provider = t.ADMIN_PROVIDER.FirstOrDefault(pr => pr.SCAMPS_PROV_ID == en.scamps_prov_id.Value);
                if (provider == null)
                    throw new SCAMPException(ErrorList[12]);
            }

            //if inpatient encounter, need valid SCAMP ID value ...
            if (en.encounter_cd == "I" && (!en.scamp_id.HasValue && !t.SCAMPs.Any(sc => sc.SCAMP_ID == en.scamp_id.Value)))
                throw new SCAMPException(ErrorList[4]);

            //if inpatient encounter and a HAR is manually given, another har for an inpatient encounter can not exist already
            if (en.encounter_cd == "I" && !string.IsNullOrEmpty(en.har) && t.PT_ENCOUNTERS.Any(enc => enc.ENCOUNTER_CD == "I" && enc.HAR.ToUpper() == en.har.ToUpper()))
                throw new SCAMPException(ErrorList[9]);

            if (en.scamp_id.HasValue && en.encounter_type.HasValue)
                encountertype = t.ADMIN_ENCOUNTERS.FirstOrDefault(enc => enc.ENCOUNTER_ID == en.encounter_type.Value && enc.SCAMP_ID == en.scamp_id.Value);

            if (encountertype == null)
                throw new SCAMPException(ErrorList[5]);

            //if HAR is manually given, generate HAR and APPT_ID
            if (string.IsNullOrEmpty(en.har))
            {
                var appthar = t.ExecuteStoreQuery<KeyValueModel>(_queryApptIdHar).FirstOrDefault();

                if (appthar == null || !long.TryParse(appthar.key, out apptid) || string.IsNullOrEmpty((HAR = appthar.value)))
                    throw new SCAMPException(ErrorList[6]);
            }
            //if no HAR is given, get server generated HAR and APPT_ID
            else
            {
                var apptgen = t.ExecuteStoreQuery<KeyValueModel>(_queryApptId).FirstOrDefault();
                if (apptgen == null || !long.TryParse(apptgen.key, out apptid))
                    throw new SCAMPException(ErrorList[6]);
                HAR = en.har.ToUpper();
            }

            //lookup location
            int? loctn_id = null;

            if (!en.loctn_abbr.IsNullOrEmpty())
            {

                var locfilter = (en.encounter_cd == "I" ? "LOCTN_INPT" : (en.encounter_cd == "O" ? "LOCTN_OUTPT" : "LOCTN_EVENT"));
                var loctn_lookup = t.ADMIN_LOOKUP.FirstOrDefault(loc => loc.LOOKUP_CD == locfilter && loc.VALUE_TXT == en.loctn_abbr);
                if (loctn_lookup == null)
                    throw new SCAMPException(ErrorList[8]);

                loctn_id = loctn_lookup.ID;
            }

            var encounter = new PT_ENCOUNTERS()
            {
                MRN = en.mrn.ToUpper(),
                APPT_ID = apptid,
                HAR = string.Concat("XX", HAR),
                ENCOUNTER_CD = en.encounter_cd,
                ENCOUNTER_TYPE = encountertype.ENCOUNTER_ID,
                ENCOUNTER_STATUS = en.encounter_status.ToString(),
                SOURCE_TXT = "FROM ePortal",
                CLINIC_NO = loctn_id
            };

            VISITS_INPT_SUBUNIT subunit = null;
            if (provider != null)
            {
                encounter.ATTENDING = provider.FULL_NAME;
                encounter.SCAMPS_PROV_ID = provider.SCAMPS_PROV_ID;
            }
            if (en.encounter_user.HasValue && t.ADMIN_PROVIDER.Any(pr => pr.SCAMPS_PROV_ID == en.encounter_user))
                encounter.ENCOUNTER_USER = en.encounter_user;

            //note: encounter_cd determines how location is stored for encounter as well as date of encounter
            switch (encounter.ENCOUNTER_CD)
            {
                case "I":
                    encounter.ENCOUNTER_TYPE = null;
                    encounter.INPT_ADMIT = en.encounter_date;
                    encounter.INPT_FLOOR = en.loctn_abbr;
                    //need to create first subunit encounter for inpatient encounter
                    subunit = new VISITS_INPT_SUBUNIT()
                    {
                        HAR = string.Concat("XX", HAR),
                        DATE_DT = en.encounter_date,
                        ENCOUNTER_TYPE = encountertype.ENCOUNTER_ID,
                        CLINIC_NAME = en.loctn_abbr,
                        SCAMP_ID = en.scamp_id.Value,
                        SEQ_VAL = 1,
                        DISPLAY_TXT = "Day-1",
                        DATA_ENTERED = "N",
                        ENCOUNTER_STATUS = en.encounter_status.ToString(),
                        ENCOUNTER_NOTES = en.encounter_notes,
                        CLINIC_NO = loctn_id
                    };
                    if (provider != null)
                    {
                        subunit.ATTENDING = provider.FULL_NAME;
                        subunit.SCAMPS_PROV_ID = provider.SCAMPS_PROV_ID;
                    }

                    t.VISITS_INPT_SUBUNIT.AddObject(subunit);
                    break;
                case "O":
                    encounter.CLINIC_APPT = en.encounter_date;
                    encounter.CLINIC_NAME = en.loctn_abbr;
                    encounter.ENCOUNTER_NOTES = en.encounter_notes;
                    break;
                default:
                    encounter.LOCTN_ABBR = en.loctn_abbr;
                    encounter.EVENT_TYPE_ABBR = en.event_type_abbr;
                    encounter.EVENT_DT_TM = en.encounter_date;
                    encounter.ENCOUNTER_NOTES = en.encounter_notes;
                    break;
            }
            //finally, save new object
            t.PT_ENCOUNTERS.AddObject(encounter);
            t.SaveChanges();
            t.Refresh(RefreshMode.StoreWins, encounter);
            if (subunit != null)
                t.Refresh(RefreshMode.StoreWins, subunit);

            new_appt_id = apptid.ToString();
        }

        public static void updateEncounter(EncounterViewModel en)
        {
            var ErrorList = new string[] { 
                "Could not find encounter to update",
                "Encounter date occurs before date of birth",
                "Invalid location",
                "Invalid provider",//3
                "HAR already exists for another inpatient encounter",//4
                "HAR exceeds maximum length",//5
                "Invalid HAR format"//6
            };

            //attempt to retrieve encounter by APPT_ID
            var t = SCAMPstore.Get();

            var encounter = t.PT_ENCOUNTERS.FirstOrDefault(e => e.APPT_ID == en.appt_id);

            if (encounter == null)
                throw new SCAMPException(ErrorList[0]);

            var HAR = string.Empty;

            if (!string.IsNullOrEmpty(en.har))
                HAR = en.har;

            //check HAR if manually set
            if (!string.IsNullOrEmpty(HAR))
            {
                if (HAR.Length > 10)
                    throw new SCAMPException(ErrorList[5]);
                //HAR sequence generated is XX200000 to XX99999999 ... prevent collisions
                //note that XX concatenated with a padded number is OK, as this won't cause a collision
                if (HAR.StartsWith("XX", StringComparison.OrdinalIgnoreCase) && !HAR.Substring(2).StartsWith("0"))
                {
                    int harnumber = 0;
                    //if har is not padded with a zero but a parsable number greater than 200000 
                    //(PT_ENCOUNTERS_HAR_SEQ is defined to start at 200001), then it is a potential collision
                    if (int.TryParse(HAR.Substring(2), out harnumber) && harnumber > 200000)
                        throw new SCAMPException(ErrorList[6]);
                }
                //if current encounter is inpatient, verify that another inpatient encounter is NOT using it
                if(encounter.ENCOUNTER_CD=="I" && t.PT_ENCOUNTERS.Any(enc => enc.ENCOUNTER_CD == "I" && enc.HAR.ToUpper() == en.har.ToUpper()))
                    throw new SCAMPException(ErrorList[4]);
            }

            //verify that encounter date does not occur BEFORE a patient is even born... if a DOB is even given for a patient
            var enpat = t.PT_MASTER.FirstOrDefault(e => e.MRN == encounter.MRN);
            if (enpat.DOB.HasValue && en.encounter_date.Value < enpat.DOB.Value)
                throw new SCAMPException(ErrorList[1]);

            //lookup location
            if (!en.loctn_abbr.IsNullOrEmpty())
            {

                var locfilter = (encounter.ENCOUNTER_CD == "I" ? "LOCTN_INPT" : (encounter.ENCOUNTER_CD == "O" ? "LOCTN_OUTPT" : "LOCTN_EVENT"));
                var loctn_lookup = t.ADMIN_LOOKUP.FirstOrDefault(loc => loc.LOOKUP_CD == locfilter && loc.VALUE_TXT == en.loctn_abbr);
                if (loctn_lookup == null)
                    throw new SCAMPException(ErrorList[2]);

                encounter.CLINIC_NO = loctn_lookup.ID;
            }
            else
                encounter.CLINIC_NO = null;


            //updating an outpatient and event encounter is different than inpatient ...
            if ("OE".Contains(encounter.ENCOUNTER_CD))
            {
                if (encounter.ENCOUNTER_CD == "O")
                {
                    encounter.CLINIC_APPT = en.encounter_date;
                    encounter.CLINIC_NAME = en.loctn_abbr;
                }
                //event
                else
                {
                    encounter.EVENT_DT_TM = en.encounter_date;
                    encounter.LOCTN_ABBR = en.loctn_abbr;
                }

                encounter.APPT_NOTES = en.appt_notes;
                encounter.PROV_LOCK_STATUS = en.prov_lock_status;
                encounter.ENCOUNTER_EMAIL = en.encounter_email;
                encounter.ENCOUNTER_PREREVIEW = en.encounter_prereview;
                encounter.ENCOUNTER_POSTREVIEW = en.encounter_postreview;
                encounter.ENCOUNTER_NOTES = en.encounter_notes;

                if (!string.IsNullOrEmpty(HAR))
                    encounter.HAR = HAR;

                //status and provider (and consequentally provider's full name for 'attending' column) need to be looked up

                //allow null values for provider but in the case that there IS a value, the provider must be valid
                var provider = t.ADMIN_PROVIDER.FirstOrDefault(pr => pr.SCAMPS_PROV_ID == en.scamps_prov_id);
                var validStatus = t.ADMIN_ENCOUNTER_STATUS.Any(st => st.STATUS_ID == en.encounter_status);

                //if no measurements for encounter exist, can change encounter type and scamp_id
                if (en.encounter_type.HasValue && t.ADMIN_ENCOUNTERS.Any(frm => frm.ENCOUNTER_ID == en.encounter_type.Value) && !t.MEASUREMENTS.Any(ms => ms.APPT_ID == en.appt_id))
                {
                    encounter.ENCOUNTER_TYPE = en.encounter_type;
                }

                if (provider != null)
                {
                    encounter.SCAMPS_PROV_ID = provider.SCAMPS_PROV_ID;
                    encounter.ATTENDING = provider.FULL_NAME;
                }
                else
                {
                    if (en.scamps_prov_id != null)
                        throw new SCAMPException(ErrorList[3]); //Throw with invalid provider.
                    else
                    {
                        encounter.SCAMPS_PROV_ID = null;
                        encounter.ATTENDING = null;
                    }
                }

                if (validStatus && en.encounter_status.HasValue)
                    encounter.ENCOUNTER_STATUS = en.encounter_status.Value.ToString();

                if (en.encounter_user.HasValue && t.ADMIN_PROVIDER.Any(pr => pr.SCAMPS_PROV_ID == en.encounter_user))
                {
                    encounter.ENCOUNTER_USER = en.encounter_user;
                }
            }
            //handling updating inpatient encounter here
            else
            {
                encounter.INPT_ADMIT = en.encounter_date;
                encounter.INPT_DISCH = en.inpt_disch;
                encounter.INPT_FLOOR = en.loctn_abbr;
                encounter.ENCOUNTER_NOTES = en.encounter_notes;
                //if updating a parent inpatient encounter, propagate HAR change to subunits
                if (!string.IsNullOrEmpty(HAR)){
                    foreach (var sub in t.VISITS_INPT_SUBUNIT.Where(v => v.HAR.ToUpper() == encounter.HAR.ToUpper()))
                        sub.HAR = HAR;
                    encounter.HAR = HAR;
                }
            }
            t.SaveChanges();
        }

        public static void updateSubUnitEncounter(EncounterViewModel en)
        {

            var ErrorList = new string[] { 
                "No subunit encounter data",
                "Could not find subunit encounter to update",
                "Encounter date occurs before patient's birth",
                "Invalid location"
            };

            if (en == null || !en.sub_id.HasValue)
                throw new SCAMPException(ErrorList[0]);

            //attempt to retrieve encounter by sub_id
            var t = SCAMPstore.Get();

            var subunit = t.VISITS_INPT_SUBUNIT.FirstOrDefault(e => e.ID == en.sub_id);

            if (subunit == null)
                throw new SCAMPException(ErrorList[1]);

            var rootencounter = t.PT_ENCOUNTERS.FirstOrDefault(e => e.HAR == subunit.HAR);

            if (rootencounter != null)
            {
                var enpat = t.PT_MASTER.FirstOrDefault(pt => pt.MRN == rootencounter.MRN);
                if (en.encounter_date.HasValue && enpat != null && enpat.DOB.HasValue && enpat.DOB.Value > enpat.DOB.Value)
                    throw new SCAMPException(ErrorList[2]);
            }

            //lookup location id
            //inpatient locations only for subunit encounters
            var locfilter = "LOCTN_INPT";
            if (!string.IsNullOrEmpty(en.loctn_abbr))
            {
                var loctn_lookup = t.ADMIN_LOOKUP.FirstOrDefault(loc => loc.LOOKUP_CD == locfilter && loc.VALUE_TXT == en.loctn_abbr);
                if (loctn_lookup == null)
                    throw new SCAMPException(ErrorList[3]);
                subunit.CLINIC_NO = loctn_lookup.ID;
                subunit.CLINIC_NAME = en.loctn_abbr;
            }
            else
            {
                subunit.CLINIC_NO = null;
                subunit.CLINIC_NAME = null;
            }

            subunit.DATE_DT = en.encounter_date;
            
            subunit.APPT_NOTES = en.appt_notes;
            subunit.PROV_LOCK_STATUS = en.prov_lock_status;
            subunit.ENCOUNTER_EMAIL = en.encounter_email;
            subunit.ENCOUNTER_PREREVIEW = en.encounter_prereview;
            subunit.ENCOUNTER_POSTREVIEW = en.encounter_postreview;
            subunit.ENCOUNTER_NOTES = en.encounter_notes;

            //status and provider (and consequentally provider's full name for 'attending' column) need to be looked up

            //allow null values for provider but in the case that there IS a value, the provider must be valid
            var validProvider = t.ADMIN_PROVIDER.FirstOrDefault(pr => pr.SCAMPS_PROV_ID == en.scamps_prov_id);
            var validEncUser = t.ADMIN_PROVIDER.FirstOrDefault(pr => pr.SCAMPS_PROV_ID == en.encounter_user);
            var validStatus = t.ADMIN_ENCOUNTER_STATUS.Any(st => st.STATUS_ID == en.encounter_status);

            //if encounter does not have measurements then its type can be changed
            if (!subunit.ENCOUNTER_TYPE.HasValue && en.encounter_type.HasValue && t.ADMIN_ENCOUNTERS.Any(frm => frm.ENCOUNTER_ID == en.encounter_type.Value) && !t.MEASUREMENTS.Any(ms => ms.HAR == subunit.HAR && ms.SUBUNIT_SEQ_VAL == subunit.SEQ_VAL))
            {
                subunit.ENCOUNTER_TYPE = en.encounter_type;
            }

            if (validProvider != null)
            {
                subunit.SCAMPS_PROV_ID = validProvider.SCAMPS_PROV_ID;
                subunit.ATTENDING = validProvider.FULL_NAME;
            }
            if (validEncUser != null)
                subunit.ENCOUNTER_USER = validEncUser.SCAMPS_PROV_ID;

            if (validStatus && en.encounter_status.HasValue)
                subunit.ENCOUNTER_STATUS = en.encounter_status.Value.ToString();

            t.SaveChanges();
        }

        //return a specific subunit encounter
        public static EncounterViewModel getSubUnitEncounter(string sub_id)
        {
            if (string.IsNullOrEmpty(sub_id))
                return null;

            int id = 0;
            if (!int.TryParse(sub_id, out id))
                return null;

            var t = SCAMPstore.Get();

            var en = t.VISITS_INPT_SUBUNIT.FirstOrDefault(sub => sub.ID == id);
            if (en == null)
                return null;

            short status_id = 99;
            short.TryParse(en.ENCOUNTER_STATUS, out status_id);

            return new EncounterViewModel()
            {
                sub_id = en.ID,
                har = en.HAR,
                encounter_cd = "I",
                encounter_date = en.DATE_DT,
                loctn_abbr = en.CLINIC_NAME,
                scamps_prov_id = en.SCAMPS_PROV_ID,
                prov_lock_status = en.PROV_LOCK_STATUS,
                scamp_id = en.SCAMP_ID,
                encounter_type = en.ENCOUNTER_TYPE,
                encounter_status = status_id,
                encounter_email = en.ENCOUNTER_EMAIL,
                encounter_prereview = en.ENCOUNTER_PREREVIEW,
                encounter_postreview = en.ENCOUNTER_POSTREVIEW,
                encounter_user = en.ENCOUNTER_USER,
                encounter_notes = en.ENCOUNTER_NOTES,
                appt_notes = en.APPT_NOTES,
                hasData = t.MEASUREMENTS.Any(ms => ms.SUBUNIT_SEQ_VAL == en.SEQ_VAL && ms.HAR == en.HAR)
            };
        }
        //return searchview of subunit encounters based on encounter id
        public static IEnumerable<EncounterSubUnitModel> getSubUnitEncounters(string appt_id)
        {

            //subid must not be empty or an unparsable integer
            if (string.IsNullOrEmpty(appt_id))
                return new EncounterSubUnitModel[0];

            long apptid = 0;
            if (!long.TryParse(appt_id, out apptid))
                return new EncounterSubUnitModel[0];

            var t = SCAMPstore.Get();
            //just given a particular sub_id, related subunit encounters can be found by doing a self-join on the column HAR
            var res = t.EN_SUBUNITS_LIST.Where(en => en.APPT_ID == apptid)
                .Select(en => new EncounterSubUnitModel()
                {
                    edata = new encounterData()
                    {
                        apptid = en.APPT_ID,
                        enstat = en.STATUSTYPE,
                        encurl = en.ENCOUNTER_URL,
                        fexist = en.WEB_FORM_APPROVED,
                        fname = en.FORM_NAME,
                        encd = "I",
                        subid = en.SUB_ID
                    },
                    sub_id = en.SUB_ID,
                    appt_id = en.APPT_ID,
                    HAR = en.HAR,
                    encDate = en.DATE_DT,
                    attending = en.ATTENDING,
                    scampName = en.SCAMPNAME,
                    encounterName = en.ENCOUNTERNAME,
                    statusDescription = en.STATUSTEXT
                });
            return res;
        }

        public static IEnumerable<SearchResultModel> getLinkedEncounters(string appt_id)
        {
            if (string.IsNullOrEmpty(appt_id))
                return new SearchResultModel[0];

            long apptid = 0;

            if (!long.TryParse(appt_id, out apptid))
                return new SearchResultModel[0];

            var t = SCAMPstore.Get();
            var result = t.ENCOUNTER_LINK.Where(lnk => lnk.SOURCE_ID == apptid)
                .Join(t.PT_EN_LIST, a => a.TARGET_ID, b => b.APPT_ID, (a, b) => b)
                .Select(pt => new SearchResultModel()
                {
                    edata = new encounterData()
                    {
                        apptid = pt.APPT_ID,
                        enstat = pt.STATUSTYPE,
                        encurl = pt.ENCOUNTER_URL,
                        fexist = pt.WEB_FORM_APPROVED,
                        fname = pt.FORM_NAME,
                        encd = pt.ENCOUNTER_CD
                    },
                    prov = pt.PROVIDERNAME,
                    date = pt.ENCOUNTERDATE,
                    scamp = pt.SCAMPNAME,
                    encounter = pt.ENCOUNTERNAME,
                    status = pt.STATUSTYPE
                });
            return result;

        }


        public static EncounterViewModel getEncounter(string appt_id)
        {
            if (string.IsNullOrEmpty(appt_id))
                return null;

            long apptid = 0;

            if (!long.TryParse(appt_id, out apptid))
                return null;

            var t = SCAMPstore.Get();

            var en = t.PT_ENCOUNTERS.FirstOrDefault(pt => pt.APPT_ID == apptid);

            int? scamp_id = null;

            if (en.ENCOUNTER_TYPE.HasValue)
            {
                int enc_id = en.ENCOUNTER_TYPE.Value;
                var ad_en = t.ADMIN_ENCOUNTERS.FirstOrDefault(ae => ae.ENCOUNTER_ID == enc_id);
                if (ad_en != null)
                    scamp_id = ad_en.SCAMP_ID;
            }

            var env = new EncounterViewModel()
            {
                mrn = en.MRN,
                har = en.HAR,
                appt_id = en.APPT_ID,
                encounter_cd = en.ENCOUNTER_CD,
                encounter_date = (en.CLINIC_APPT.HasValue ? en.CLINIC_APPT : en.EVENT_DT_TM.HasValue ? en.EVENT_DT_TM : en.INPT_ADMIT),
                inpt_disch = en.INPT_DISCH,
                loctn_abbr = (string.IsNullOrEmpty(en.LOCTN_ABBR) ? (string.IsNullOrEmpty(en.CLINIC_NAME) ? en.INPT_FLOOR : en.CLINIC_NAME) : en.LOCTN_ABBR),
                scamps_prov_id = en.SCAMPS_PROV_ID,
                prov_lock_status = en.PROV_LOCK_STATUS,
                scamp_id = scamp_id,
                //esstr = en.ENCOUNTER_STATUS,
                encounter_type = en.ENCOUNTER_TYPE,
                event_type_abbr = en.EVENT_TYPE_ABBR,
                encounter_email = en.ENCOUNTER_EMAIL,
                encounter_prereview = en.ENCOUNTER_PREREVIEW,
                encounter_postreview = en.ENCOUNTER_POSTREVIEW,
                encounter_user = en.ENCOUNTER_USER,
                encounter_notes = en.ENCOUNTER_NOTES,
                appt_notes = en.APPT_NOTES,
                hasData = !en.ENCOUNTER_CD.Equals("I") && t.MEASUREMENTS.Any(ms => ms.APPT_ID == en.APPT_ID)
            };

            if (!string.IsNullOrEmpty(en.ENCOUNTER_STATUS))
            {
                short tmp;
                if (short.TryParse(en.ENCOUNTER_STATUS, out tmp))
                    env.encounter_status = tmp;
            }

            return env;

        }

        //returns encounters for a patient given their MRN
        public static IEnumerable<PatientEncounterModel> getPatientEncounters(string mrn)
        {
            if (string.IsNullOrEmpty(mrn))
                return new PatientEncounterModel[0];

            var t = SCAMPstore.Get();
            var result = t.PT_EN_LIST.Where(pt => pt.MRN.ToUpper() == mrn.ToUpper())
                .Select(pt => new PatientEncounterModel()
                {
                    edata = new encounterData()
                    {
                        apptid = pt.APPT_ID,
                        enstat = pt.STATUSTYPE,
                        encurl = pt.ENCOUNTER_URL,
                        fexist = pt.WEB_FORM_APPROVED,
                        fname = pt.FORM_NAME,
                        encd = pt.ENCOUNTER_CD
                    },
                    encounterStatus = pt.STATUSTEXT,
                    provider = pt.PROVIDERNAME,
                    encounterDate = pt.ENCOUNTERDATE,
                    dischargeDate = pt.DISCHARGEDATE,
                    visitType = pt.VTYPE,
                    ageMinutes = pt.AGEMINUTE,
                    location = pt.LOCATION,
                    scampName = pt.SCAMPNAME,
                    encounterName = pt.ENCOUNTERNAME,
                    statusType = pt.STATUSTYPE,
                    encounterLinkID = pt.LINK_ID,
                    subunitID = pt.SUB_ID
                });
            return result;

        }


        #region encounterSearchFiltering

        private static IQueryable<VW_ENCOUNTER_SEARCH> filterListExact(EncounterSearch emodel)
        {
            var t = SCAMPstore.Get();
            //var q = t.ENCOUNTER_SEARCHVIEW.AsQueryable();
            var q = t.VW_ENCOUNTER_SEARCH.AsQueryable();

            //by patient criteria
            if (!emodel.MRN.IsNullOrEmpty())
                q = q.Where(p => p.MRN.ToLower() == emodel.MRN.ToLower());
            if (!emodel.HAR.IsNullOrEmpty())
                q = q.Where(p => p.HAR.ToLower() == emodel.HAR.ToLower());
            if (!emodel.firstname.IsNullOrEmpty())
                q = q.Where(p => p.FNAME.ToLower() == emodel.firstname.ToLower());
            if (!emodel.lastname.IsNullOrEmpty())
                q = q.Where(p => p.LNAME.ToLower() == emodel.lastname.ToLower());
            if (!emodel.gender.IsNullOrEmpty())
                q = q.Where(p => p.SEX.ToLower() == emodel.gender.ToLower());
            if (emodel.DOB.HasValue)
                q = q.Where(p => p.DOB.HasValue && p.DOB.Value.Day == emodel.DOB.Value.Day && p.DOB.Value.Month == emodel.DOB.Value.Month && p.DOB.Value.Year == emodel.DOB.Value.Year);

            //predicates related to the encounter are now built. These are never fuzzy.

            return q;
        }

        //private static IQueryable<ENCOUNTER_SEARCHVIEW> filterListFuzzy(EncounterSearch emodel)
        private static IQueryable<VW_ENCOUNTER_SEARCH> filterListFuzzy(EncounterSearch emodel)
        {
            var t = SCAMPstore.Get();
            //var q = t.ENCOUNTER_SEARCHVIEW.AsQueryable();
            var q = t.VW_ENCOUNTER_SEARCH.AsQueryable();

            //by patient criteria
            if (!emodel.HAR.IsNullOrEmpty())
                q = q.Where(p => p.HAR.ToLower().Contains(emodel.HAR.ToLower()));
            if (!emodel.MRN.IsNullOrEmpty())
                q = q.Where(p => p.MRN.ToLower().Contains(emodel.MRN.ToLower()));
            if (!emodel.firstname.IsNullOrEmpty())
                q = q.Where(p => p.FNAME.ToLower().Contains(emodel.firstname.ToLower()));
            if (!emodel.lastname.IsNullOrEmpty())
                q = q.Where(p => p.LNAME.ToLower().Contains(emodel.lastname.ToLower()));
            if (!emodel.gender.IsNullOrEmpty())
                q = q.Where(p => p.SEX.ToLower().Contains(emodel.gender.ToLower()));
            if (emodel.DOB.HasValue)
                q = q.Where(p => p.DOB.HasValue && p.DOB.Value.Day == emodel.DOB.Value.Day && p.DOB.Value.Month == emodel.DOB.Value.Month && p.DOB.Value.Year == emodel.DOB.Value.Year);

            //predicates related to the encounter are now built. These are never fuzzy.

            return q;
        }

        //private static IQueryable<ENCOUNTER_SEARCHVIEW> finalEncounterFilters(IQueryable<ENCOUNTER_SEARCHVIEW> q, EncounterSearch emodel)
        private static IQueryable<VW_ENCOUNTER_SEARCH> finalEncounterFilters(IQueryable<VW_ENCOUNTER_SEARCH> q, EncounterSearch emodel)
        {
            if (emodel.PID.HasValue)
                q = q.Where(p => p.PID == emodel.PID.Value);
            if (emodel.providerID.HasValue)
                q = q.Where(p => p.SCAMPS_PROV_ID.HasValue && p.SCAMPS_PROV_ID == emodel.providerID);
            if (!emodel.visitType.IsNullOrEmpty())
                q = q.Where(p => p.ENCOUNTER_CD.ToLower() == emodel.visitType.ToLower());
            if (emodel.SCAMP.HasValue)
                q = q.Where(p => p.SCAMP_ID.HasValue && p.SCAMP_ID == emodel.SCAMP.Value);
            if (emodel.start.HasValue)
                q = q.Where(p => p.EN_DATE.HasValue && p.EN_DATE >= emodel.start);
            if (emodel.end.HasValue)
                q = q.Where(p => p.EN_DATE.HasValue && p.EN_DATE <= emodel.end);
            if (!string.IsNullOrEmpty(emodel.location))
                q = q.Where(p => p.LOCATION.ToLower() == emodel.location.ToLower());

            //apply status filter here
            /* status search filter flags: if bit is set, then status type is included
             * 1. COMPLETED
             * 2. IN_PROGRESS_INCOMPLETE
             * 3. IN_PROGRESS_NO_DATA
             * 4. IN_PROGRESS_FOR_REVIEW
             * 5. IN_PROGRESS_INCOMPLETE_SCANNED
             * 6. NOT_SCAMP_VISIT
             */
            if (emodel.statusTypeFlag.HasValue && emodel.statusTypeFlag.Value > 0)
            {
                if (emodel.statusTypeFlag.HasValue && emodel.statusTypeFlag.Value > 0)
                {
                    var statusList = new List<string>();
                    var flagVal = emodel.statusTypeFlag.Value;
                    if ((flagVal & 1) == 1)
                        statusList.Add("COMPLETED");
                    if ((flagVal & 2) == 2)
                        statusList.Add("IN_PROGRESS_INCOMPLETE");
                    if ((flagVal & 4) == 4)
                        statusList.Add("IN_PROGRESS_NO_DATA");
                    if ((flagVal & 8) == 8)
                        statusList.Add("IN_PROGRESS_FOR_REVIEW");
                    if ((flagVal & 16) == 16)
                        statusList.Add("IN_PROGRESS_INCOMPLETE_SCANNED");
                    if ((flagVal & 32) == 32)
                        statusList.Add("NOT_SCAMP_VISIT");

                    q = q.Where(p => statusList.Contains(p.STATUS_TYPE));
                }
            }

            //apply disposition filters here. Disposition has some more complex predicates based on its value
            if (emodel.disposition > 0)
            {
                q = q.Where(p => p.STATUS_VAL.HasValue);
                switch (emodel.disposition)
                {
                    case 1://TempExcluded
                        q = q.Where(en => en.STATUS_VAL == 0);
                        break;
                    case 2://PermExcluded
                        q = q.Where(en => en.STATUS_VAL == -1);
                        break;
                    case 3://Exited
                        q = q.Where(en => en.STATUS_VAL == -2);
                        break;
                    case 4://EncounterWithin14
                        q = q.Where(en => en.STATUS_VAL > 0 && en.EN_DATE.HasValue && en.EN_DATE.Value > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -14));
                        break;
                    case 5://included
                        q = q.Where(en => en.STATUS_VAL == 1);
                        break;
                    case 6://toBeScreened
                        q = q.Where(en => en.STATUS_VAL == 2);
                        break;
                    case 7://toBeRescreened
                        q = q.Where(en => en.STATUS_VAL == 3);
                        break;
                    case 8://dischargedWithin7
                        q = q.Where(en => en.STATUS_VAL > 0 && en.INPT_DISCH.HasValue && en.INPT_DISCH.Value > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -7));
                        break;
                    case 9://IncludedFlagged
                        q = q.Where(en => en.STATUS_VAL == 1 && en.FLAG_STATUS == -1);
                        break;
                    case 10://CurrentInpatients
                        q = q.Where(en => en.STATUS_VAL > 0 && en.INPT_ADMIT.HasValue && en.INPT_ADMIT.Value > EntityFunctions.AddYears(EntityFunctions.TruncateTime(DateTime.Now), -1) && !en.INPT_DISCH.HasValue);
                        //enlist = enlist.Where(en => en.STATUS_VAL > 0 && en.INPT_ADMIT > DbFunctions.AddYears(DateTime.UtcNow,-1));
                        break;
                    case 11://CompletedWebForms
                        q = q.Where(en => en.STATUS_VAL.HasValue && en.STATUS_VAL == 51);
                        //q = q.Where(en => en.STATUS_VAL >= 0 && (en.STATUS_VAL == 51 || en.STATUS_VAL.Equals("51")));
                        break;
                    default:
                        break;
                }
            }
            //limit search to maxresults parameter set in search model
            if (emodel.maxresults > 0)
                q = q.Take(emodel.maxresults);
            return q;
        }

        #endregion
        //gets a collection of patient encounters. Formatted for the search page.
        //since this is a two dimensional array, the first dimension (e.g. list[i]) is the 'row'
        //the second dimension (e.g. list[i][j]) is the value of a for a row
        //**UPDATE now returns a SearchResultModel instead of a string array
        public static IEnumerable<SearchResultModel> getList(EncounterSearch emodel)
        {
            if (emodel == null || !emodel.hasParameters())
                return new SearchResultModel[0];

            IQueryable<VW_ENCOUNTER_SEARCH> q = emodel.exactSearch ? filterListExact(emodel) : filterListFuzzy(emodel);
            //IQueryable<ENCOUNTER_SEARCHVIEW> q = emodel.exactSearch ? filterListExact(emodel) : filterListFuzzy(emodel);

            q = finalEncounterFilters(q, emodel);

            if (emodel.maxresults > 0)
                q = q.Take(emodel.maxresults);

            return q.Select(r => new SearchResultModel()
            {
                pdata = "icon-patLocal",
                edata = new encounterData()
                {
                    apptid = r.APPT_ID,
                    //enstat = r.ENCOUNTER_STATUS,
                    enstat = r.STATUS_TEXT,
                    encurl = r.ENCOUNTER_URL,
                    //fexist = r.FORM_EXISTS,
                    fexist = r.FORMEXISTS,
                    fname = r.FORM_NAME,
                    encd = r.ENCOUNTER_CD,
                    //subid = r.SUBUNIT_ID
                    subid = r.SUB_ID
                },
                mrn = r.MRN,
                lname = r.LNAME,
                fname = r.FNAME,
                dob = r.DOB,
                gender = r.SEX,
                prov = r.ATTENDING,
                location = r.LOCATION,
                //location = r.CLINIC_NAME,
                date = r.EN_DATE,
                //date = (r.ENCOUNTER_CD.Equals("E") ? r.EVENT_DT_TM :
                //    (r.ENCOUNTER_CD.Equals("I") ? r.INPT_ADMIT : r.CLINIC_APPT)),
                //visit = (r.ENCOUNTER_CD.Equals("E") ? (string.IsNullOrEmpty(r.EVENT_TYPE_ABBR) ? "event" : r.EVENT_TYPE_ABBR + " event") : r.ENCOUNTER_CD.Equals("I") ? "Inpatient" : "Outpatient"),
                visit = r.VISIT_TEXT,
                scamp = r.SCAMP_NAME,
                encounter = r.ENCOUNTER_NAME_SHORT,
                status = r.STATUS_TEXT
            });
        }

        /* this method builds the display markup for the encounter link
            all of the criteria here is based on the original encounter search view
            this needs to be rewritten so that the markup can be assembled client-side
         */
        /* DEPRICATED */
        /*
        private static string getSDFmarkup(ENCOUNTER_SEARCHVIEW svObj)
        {
            //NOTE: need to know if form is approved, which is determined by this statement:

             // select distinct f.form_name
             // from admin_form_fields f,
             //      admin_encounters e
             // where upper(f.form_name) = upper(e.form_name) and
             //      upper(e.web_form_approved) = 'Y'
             // order by f.form_name
            

            //
            // encounter_status, ENCOUNTER_URL, form_exists, form_name, encounter_cd, subunit_id
            //

            string en_status = svObj.ENCOUNTER_STATUS;
            bool isPDF = (!string.IsNullOrEmpty(svObj.ENCOUNTER_URL) && svObj.ENCOUNTER_URL.ToLower().Contains("pdf"));
            bool webReady = svObj.FORM_EXISTS == 1;

            const string URLtemplate = "href=\"/Forms/FSDF.aspx?SDF={0}&APPT_ID={1}&VISIT_TYPE={2}\" target=\"_new\" ";
            
            string sdfURL = string.Empty;
            if(webReady){
                sdfURL = string.Format(URLtemplate, svObj.FORM_NAME, svObj.APPT_ID.ToString(), svObj.ENCOUNTER_CD);
                if (svObj.ENCOUNTER_CD.ToUpper().Equals("I") && svObj.SUBUNIT_ID.HasValue)
                    sdfURL = sdfURL + "&SUBUNIT_ID="+svObj.SUBUNIT_ID.Value.ToString();
            }
            else if (isPDF)
            {
                sdfURL = string.Format("href=\"{0}\" ",svObj.ENCOUNTER_URL);
            }

            bool hasSDFlink = false;

            const string SDFtemplate = "<a {0}class=\"{1}\" title=\"{2}\"></a>";
            string eclass = "eicon";
            string iclass = string.Empty;
            string tooltip = string.Empty;
            string iconclass = string.Empty;

            switch (en_status)
            {
                case null:
                case ""://blank case, show nothing 
                    break;
                case "IN_PROGRESS_INCOMPLETE_SCANNED":
                    iclass = " ipacket";
                    tooltip = "View scanned packet";
                    break;
                case "NOT_SCAMP_VISIT":
                    iclass = " iNA";
                    tooltip = "Not a SCAMPs visit";
                    break;
                case "COMPLETED":
                    iclass = webReady ? " iedit" : " ilock";
                    tooltip = webReady ? "Encounter Completed, open WebForm" : "Encounter Completed, no WebForm";
                    hasSDFlink = webReady;
                    break;
                case "IN_PROGRESS_NO_DATA":
                    iclass = webReady
                                               ? " iedit"
                                               : (isPDF ? " ipdf" : string.Empty);
                    tooltip = webReady ? "Edit WebForm" : "WebForm not available, view PDF";
                    hasSDFlink = webReady;
                    break;
                default:
                    iclass = webReady
                                               ? " iedit"
                                               : (isPDF ? " ipdf" : string.Empty);
                    tooltip = webReady ? "Encounter In Progress, open WebForm" : "WebForm not available, view PDF";
                    break;
            }
            return string.Format(SDFtemplate, sdfURL, (eclass + iclass), tooltip);
        }
       */

    }
}
