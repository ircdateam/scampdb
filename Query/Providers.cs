﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;

namespace ScampsDB.Query
{
    public static class Providers
    {
        public static int getProviderIDbyUserName(string username)
        {
            int pid = -1;
            if (string.IsNullOrEmpty(username))
                return pid;

            var t = SCAMPstore.Get();

            try
            {
                pid = t.ADMIN_PROVIDER.Where(p => p.USER_NAME.ToLower() == username.ToLower()).Select(p => p.SCAMPS_PROV_ID).FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Unable to get provider id for username '{0}': {1}", username, e.ToString());
                return -1;
            }
            return pid;
        }
    }
}
