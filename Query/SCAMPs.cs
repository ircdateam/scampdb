﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;
using System.Data.Objects;

namespace ScampsDB.Query
{
    public static class Scamps
    {
        /* return codes for patientEnrollment
         * 0 - success
         * 1 - invalid input
         * 2 - invalid scamp id or scamp status value
         * 3 - can not enroll patient that doesn't exist in database
         * 4 - could not add scamp status (db exception)
         * 5 - could not add screening information (db exception)
         * 6 - enrollment comment required
         */
        //enrolls a patient into a SCAMP

        private static string[] EnrollErrorList = new string[] { 
                "an enrollment reason is required",
                "could not identify patient record for enrollment",
                "invalid enrollment action code",
                "invalid enrollment action for scamp",
                "patient does not exist"

            };

        public static void patientEnrollment(string currUser, string mrn, string scampName, string status, string enrollComment)
        {

            var ctx = SCAMPstore.Get();

            if (string.IsNullOrEmpty(mrn) || string.IsNullOrEmpty(scampName) || string.IsNullOrEmpty(status))
                throw new SCAMPException(EnrollErrorList[1]);

            short statusval = 0;
            int scampID = 0;

            if (!short.TryParse(status, out statusval))
                throw new SCAMPException(EnrollErrorList[2]);

            var scamp = ctx.SCAMPs.FirstOrDefault(s => s.SCAMP_NAME == scampName);

            if (scamp == null || scamp.SCAMP_ID < 1)
                throw new SCAMPException(EnrollErrorList[3]);
            else
                scampID = scamp.SCAMP_ID;

            patientEnrollment(currUser, mrn, scampID, statusval, enrollComment);
        }

        public static void patientEnrollment(string currUser, string mrn, int scampID, short status, string enrollComment)
        {
            if (string.IsNullOrEmpty(enrollComment))
                throw new SCAMPException(EnrollErrorList[0]);

            if (string.IsNullOrEmpty(mrn))
                throw new SCAMPException(EnrollErrorList[1]);

            //if (!ctx.ADMIN_SCAMPS_STATUS.Any(w => w.SCAMP.SCAMP_NAME == scampName && w.STATUS_VAL == statusval))
            if (scampID < 1)
                throw new SCAMPException(EnrollErrorList[3]);

            var ctx = SCAMPstore.Get();

            //patient's existence
            if (!ctx.PT_MASTER.Any(w => w.MRN.ToUpper() == mrn.ToUpper()))
                throw new SCAMPException(EnrollErrorList[4]);

            //work with pt_scamp_status

            //will return a 'list' of scamp statuses with the matching mrn and scamp id...there really should only be one result returned
            var se = ctx.PT_SCAMP_STATUS.FirstOrDefault(s => s.MRN.ToUpper() == mrn.ToUpper() && s.SCAMP.SCAMP_ID == scampID);

            PT_SCAMP_STATUS newStat = null;
            short? oldstatusval = null;

            //enroll patient if currently not enrolled
            if (se == null)
            {

                newStat = new PT_SCAMP_STATUS()
                {
                    MRN = mrn.ToUpper(),
                    SCAMP_ID = scampID,
                    STATUS_VAL = status
                };

                ctx.PT_SCAMP_STATUS.AddObject(newStat);
            }
            //otherwise, update enrollment status
            else
            {
                oldstatusval = se.STATUS_VAL;
                se.STATUS_VAL = status;
            }

            //create admin_screening entry regarding enrollment
            var screening = new ADMIN_SCREENING()
            {
                SCAMP_ID = scampID,
                AUDIT_DT_TM = DateTime.Now,
                USER_ID = currUser,
                MRN = mrn.ToUpper(),
                OLD_VAL = oldstatusval.HasValue ? oldstatusval.ToString() : null,
                NEW_VAL = status.ToString(),
                CMNT_TXT = enrollComment
            };

            ctx.ADMIN_SCREENING.AddObject(screening);
            ctx.SaveChanges();
            ctx.Refresh(RefreshMode.StoreWins, screening);
            if (newStat != null)
                ctx.Refresh(RefreshMode.StoreWins, newStat);
        }

        //withdrawls a patient from a scamp enrollment
        public static void withdrawPatient(string mrn, string scampName, string currUser, string enrollComment)
        {

            var ErrorList = new string[] { 
                "Could not find MRN",
                "could not find SCAMP",
                "Patient is not enrolled in SCAMP",
                "Missing withdrawl reason"
            };

            if (string.IsNullOrEmpty(mrn))
                throw new SCAMPException(ErrorList[0]);
            if (string.IsNullOrEmpty(scampName))
                throw new SCAMPException(ErrorList[1]);
            if (string.IsNullOrEmpty(enrollComment))
                throw new SCAMPException(ErrorList[3]);

            var ctx = SCAMPstore.Get();
            var se = ctx.PT_SCAMP_STATUS.FirstOrDefault(s => s.MRN.ToUpper() == mrn.ToUpper() && s.SCAMP.SCAMP_NAME == scampName);

            if (se == null)
                throw new SCAMPException(ErrorList[2]);

            var scampval = se.SCAMP_ID;
            var oldstatusval = se.STATUS_VAL.ToString();
            ctx.PT_SCAMP_STATUS.DeleteObject(se);

            var screening = new ADMIN_SCREENING()
            {
                SCAMP_ID = scampval,
                AUDIT_DT_TM = DateTime.Now,
                USER_ID = currUser,
                MRN = mrn.ToUpper(),
                OLD_VAL = oldstatusval,
                CMNT_TXT = enrollComment
            };

            ctx.ADMIN_SCREENING.AddObject(screening);
            ctx.SaveChanges();
            ctx.Refresh(RefreshMode.StoreWins, screening);
        }

        public static IEnumerable<SCAMPenrollHistModel> getPatientEnrollmentHistory(string mrn)
        {
            if (string.IsNullOrEmpty(mrn))
                return new SCAMPenrollHistModel[0];

            var t = SCAMPstore.Get();

            return t.ADMIN_SCREENING.Where(scr => scr.MRN.ToLower() == mrn.ToLower())
                .OrderBy(scr => scr.AUDIT_DT_TM)
                .Join(t.SCAMPs, a => a.SCAMP_ID, b => b.SCAMP_ID,
                (a, b) => new SCAMPenrollHistModel()
                {
                    mrn = a.MRN,
                    audit_dt_tm = a.AUDIT_DT_TM,
                    comment = a.CMNT_TXT,
                    scamp_name = b.SCAMP_NAME,
                    user_id = a.USER_ID,
                    new_val = a.NEW_VAL,
                    old_val = a.OLD_VAL
                });
        }

        //gets a list of SCAMPs a patient is enrolled in
        public static IEnumerable<SCAMPenrollModel> getPatientSCAMPenrollment(string mrn)
        {
            var t = SCAMPstore.Get();
            var result = t.VW_PT_CURR_ENROLL.Where(en => en.MRN.ToLower() == mrn.ToLower());

            if (result.Count() == 0)
            {

                return t.SCAMPs.Where(sc => string.IsNullOrEmpty(sc.VISIBLE) || sc.VISIBLE == "Y").Select(sc => new SCAMPenrollModel()
                {
                    scamp_id = sc.SCAMP_ID,
                    scamp_name = sc.SCAMP_NAME,
                    mrn = mrn,
                    status_val = null,
                    cmnt_txt = null,
                    user_id = null,
                    audit_dt_tm = null
                });
            }

            return result.Select(en => new SCAMPenrollModel()
            {
                scamp_id = en.SCAMP_ID,
                scamp_name = en.SCAMP_NAME,
                mrn = en.MRN,
                status_val = en.STATUS_VAL,
                cmnt_txt = en.CMNT_TXT,
                user_id = en.USER_ID,
                audit_dt_tm = en.AUDIT_DT_TM
            });
        }

        public static bool isPatientEnrolledInSCAMP(string mrn, int scamp_id)
        {
            var t = SCAMPstore.Get();
            return t.VW_PT_CURR_ENROLL.Any(en => en.MRN.ToLower() == mrn.ToLower() && en.SCAMP_ID == scamp_id && en.STATUS_VAL == 1);
        }
    }
}
