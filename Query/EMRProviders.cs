﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;
using System.Data.Objects;

namespace ScampsDB.Query
{
    public static class EMRProviders
    {
        public static IEnumerable<EMRProviderModel> searchProviders(string searchterm, bool exact = false)
        {
            IEnumerable<EMRProviderModel> provlist = new EMRProviderModel[0];
            if (string.IsNullOrEmpty(searchterm))
                return provlist;

            bool isNumeric = false;
            bool isEmail = false;

            searchterm = searchterm.Replace('*', '%').ToUpper();

            isNumeric = searchterm.All(ch => char.IsNumber(ch) || ch == '%');
            isEmail = searchterm.Any(ch => ch == '@');

            var t = new EMREntities();

            IQueryable<PROV_CNTCT_SUPPLMT> cntct = null;
            IQueryable<INDVDL_PROV> provs = null;

            if (exact)
            {
                //filter out '%' if exact search
                searchterm = new string(searchterm.TakeWhile(ch => ch != '%').ToArray());
                int num = 0;
                if (isNumeric && int.TryParse(searchterm, out num))
                {
                    provs = t.INDVDL_PROV.Where(pr => pr.PROV_ID == num || pr.CH_EMP_ID == num);
                }
                else if (isEmail)
                {
                    cntct = t.PROV_CNTCT_SUPPLMT.Where(pr => pr.CNTCT_TYPE_CD == "EML" && pr.CNTCT_TEXT.ToUpper() == searchterm);
                }
                else
                {
                    var terms = searchterm.Split(' ');
                    //the Lambda queries don't like accessing the terms array ... 
                    string fname, lname, initial;

                    if (terms.Length > 0)
                    {
                        fname = terms[0];
                        provs = t.INDVDL_PROV.Where(pr => pr.FIRST_NAME.ToUpper() == fname);
                    }
                    if (terms.Length > 1)
                    {
                        lname = terms[1];
                        provs = provs.Where(pr => pr.LAST_NAME.ToUpper() == lname);
                    }
                    if (terms.Length > 2)
                    {
                        initial = terms[2];
                        provs = provs.Where(pr => pr.MID_INITL.ToUpper() == initial);
                    }
                }
            }
            else
            {
                if (isNumeric)
                {
                    provs = t.INDVDL_PROV.Where("CAST(it.PROV_ID as System.String) LIKE @searchterm", new ObjectParameter("searchterm", searchterm));
                }
                else if (isEmail)
                {
                    //cntct = t.PROV_CNTCT_SUPPLMT.Where(pr => pr.CNTCT_TYPE_CD == "EML" && SqlMethods.Like(pr.CNTCT_TEXT.ToLower(), searchterm));
                    searchterm = searchterm.ToLower();
                    cntct = t.PROV_CNTCT_SUPPLMT.Where("it.CNTCT_TEXT like @searchterm", new ObjectParameter("searchterm", searchterm));
                }
                else
                {
                    var terms = searchterm.Split(' ');
                    if (terms.Length == 1)
                    {
                        //provs = t.INDVDL_PROV.Where(pr => SqlMethods.Like(pr.FIRST_NAME.ToLower(), terms[0]));
                        provs = t.INDVDL_PROV.Where("it.FIRST_NAME LIKE @fname", new ObjectParameter("fname", terms[0]));
                    }
                    else if (terms.Length == 2)
                    {
                        //provs = provs.Where(pr => SqlMethods.Like(pr.LAST_NAME.ToLower(), terms[1]));
                        provs = t.INDVDL_PROV.Where("it.FIRST_NAME like @fname AND it.LAST_NAME LIKE @lname", new ObjectParameter("fname", terms[0]), new ObjectParameter("lname", terms[1]));
                    }
                    else if (terms.Length > 2)
                    {
                        //provs = provs.Where(pr => SqlMethods.Like(pr.MID_INITL.ToLower(), terms[2]));
                        provs = t.INDVDL_PROV.Where("it.FIRST_NAME like @fname AND it.LAST_NAME LIKE @lname and it.MID_INITL like @mid", new ObjectParameter("fname", terms[0]), new ObjectParameter("lname", terms[1]), new ObjectParameter("mid", terms[2]));
                    }
                }
            }


            if (cntct != null)
            {
                provlist = cntct
                        .Join(t.INDVDL_PROV, a => a.INDVDL_PROV_ID, b => b.PROV_ID, (a, b) => new { idvl = b, eml = a.CNTCT_TEXT })
                        .GroupJoin(t.PROVs, a => a.idvl.PROV_ID, b => b.EPDS_NUM, (a, b) => new { idvl = a.idvl, eml = a.eml, cowname = b.Select(x => x.FULL_NAME).DefaultIfEmpty() })
                        .SelectMany(a => a.cowname.Select(b => new { idvl = a.idvl, eml = a.eml, cowname = b }))
                    //finally sort list
                        .OrderBy(ii => ii.idvl.LAST_NAME)
                        .Select(rec => new EMRProviderModel()
                        {
                            prov_id = rec.idvl.PROV_ID,
                            fname = rec.idvl.FIRST_NAME,
                            lname = rec.idvl.LAST_NAME,
                            initial = rec.idvl.MID_INITL,
                            suffix = rec.idvl.NAME_SUFFIX,
                            credentials = rec.idvl.PROV_CREDENTIALS,
                            email = rec.eml,
                            emp_id = rec.idvl.CH_EMP_ID,
                            cowname = rec.cowname,
                            active = rec.idvl.PROV_STATUS.Equals("A")
                        });
            }
            else if (provs != null)
            {
                provlist = provs.GroupJoin(t.PROV_CNTCT_SUPPLMT, a => a.PROV_ID, b => b.INDVDL_PROV_ID, (a, b) => new { idvl = a, eml = b.Where(yy => yy.CNTCT_TYPE_CD == "EML").Select(x => x.CNTCT_TEXT).DefaultIfEmpty() })
                        .SelectMany(a => a.eml.Select(b => new { idvl = a.idvl, eml = b }))
                    //left outer join using provider id
                        .GroupJoin(t.PROVs, a => a.idvl.PROV_ID, b => b.EPDS_NUM, (a, b) => new { idvl = a.idvl, eml = a.eml, cowname = b.Select(x => x.FULL_NAME).DefaultIfEmpty() })
                        .SelectMany(a => a.cowname.Select(b => new { idvl = a.idvl, eml = a.eml, cowname = b }))
                    //finally sort list
                        .OrderBy(ii => ii.idvl.LAST_NAME)
                        .Select(rec => new EMRProviderModel()
                        {
                            prov_id = rec.idvl.PROV_ID,
                            fname = rec.idvl.FIRST_NAME,
                            lname = rec.idvl.LAST_NAME,
                            initial = rec.idvl.MID_INITL,
                            suffix = rec.idvl.NAME_SUFFIX,
                            credentials = rec.idvl.PROV_CREDENTIALS,
                            email = rec.eml,
                            emp_id = rec.idvl.CH_EMP_ID,
                            cowname = rec.cowname,
                            active = rec.idvl.PROV_STATUS.Equals("A")
                        });
            }

            return provlist;
        }

        public static IEnumerable<string[]> getList(string fname, string lname, bool exact)
        {
            //return empty array if first and last name are empty
            if (string.IsNullOrEmpty(fname) && string.IsNullOrEmpty(lname))
                return new string[0][];

            //instantiate EMR db model
            var t = new EMREntities();


            /*
             * fwild and lwild are used to determine placement of wildcards in the two search parameters firstname and lastname
             *  they operate as bit flag containers
             * -- 1st bit (1) representing the string having a wildcard character at the start of the string
             * -- second bit (2) representing a wildcard char at the end
             * this allows for easy testing of all 4 cases: no wild cards, left wildcard, right wildcard, and two wildcards
             * the testing can be done with bit-wise comparators : e.g. 
             * -- (fwild & 1) == 1 determines if there is a right wildcard in firstname
             * -- (fwild & 2) == 1 determines if there is a left wildcard in firstname
             */
            int fwild = 0;
            int lwild = 0;
            if (fname != null)
            {
                if (fname.StartsWith("*"))
                    fwild += 2;
                if (fname.EndsWith("*"))
                    fwild++;
            }
            if (lname != null)
            {
                if (lname.StartsWith("*"))
                    lwild += 2;

                if (lname.EndsWith("*"))
                    lwild++;
            }

            //removal of wildcards from the search parameters
            if (fwild > 0)
                fname = fname.Replace("*", "");
            if (lwild > 0)
                lname = lname.Replace("*", "");

            //this is a very nasty data entity representation of a wildcard query which takes care of the cases where
            //we may or may not do an exact search on these fields and the fields we search for may be undefined (e.g. search firstname only)
            var rset = t.INDVDL_PROV

                //check optional field firstname 

                /*note: this nested block below takes care of the processing for determining if this search
                 * parameter 'firstname' is null, which would make it not a part of the search criteria (e.g. maybe searching by lastname only)
                 * takes care of whether or not we are doing an exact search on this term
                 * also takes care of the wildcard placement permutations (*Joe or Joe* or *Joe*)
                 * this is necessary as there does not appear to be a nicer way of doing this in lamda statements
                 */
                .Where(en => string.IsNullOrEmpty(fname) ||
                    //if we are doing an exact search *AND* first name parameter is given to us ...
                    (exact ?
                    !string.IsNullOrEmpty(en.FIRST_NAME) && en.FIRST_NAME.ToLower().Equals(fname.ToLower()) :
                    !string.IsNullOrEmpty(en.FIRST_NAME) &&
                    (fwild > 1 ?
                        (fwild == 3 ?
                            en.FIRST_NAME.ToLower().Contains(fname.ToLower()) :
                            en.FIRST_NAME.ToLower().EndsWith(fname.ToLower())
                        ) :
                        en.FIRST_NAME.ToLower().StartsWith(fname.ToLower()))
                    )
                )
                //check optional field lastname
                .Where(en => string.IsNullOrEmpty(lname) ||
                    //if we are doing an exact search *AND* last name parameter is given to us ...
                    (exact ?
                    !string.IsNullOrEmpty(en.LAST_NAME) && en.LAST_NAME.ToLower().Equals(lname.ToLower()) :
                    !string.IsNullOrEmpty(en.LAST_NAME) &&
                    (lwild > 1 ?
                        (lwild == 3 ?
                            en.LAST_NAME.ToLower().Contains(lname.ToLower()) :
                            en.LAST_NAME.ToLower().EndsWith(lname.ToLower())
                        ) :
                        en.LAST_NAME.ToLower().StartsWith(lname.ToLower()))
                    )
                )
                //.Where(en => string.IsNullOrEmpty(fname) || ( exact ? !string.IsNullOrEmpty(en.FIRST_NAME) && en.FIRST_NAME.ToLower().Equals(fname.ToLower()) : !string.IsNullOrEmpty(en.FIRST_NAME) && en.FIRST_NAME.ToLower().Contains(fname.ToLower()) ) )
                //.Where(en => string.IsNullOrEmpty(lname) || ( exact ? !string.IsNullOrEmpty(en.LAST_NAME) && en.LAST_NAME.ToLower().Equals(lname.ToLower()) : !string.IsNullOrEmpty(en.LAST_NAME) && en.LAST_NAME.ToLower().Contains(lname.ToLower()) ) )

                /******************************
                * 'where' criteria ends here, now we just join our result set with the desired supplimentary data from other tables using left joins 
                * (as the supplimentary criteria may not even exist, but we still want to see results anyway)
                ******************************/

                //left outer join using provider id
                .GroupJoin(t.PROV_CNTCT_SUPPLMT, a => a.PROV_ID, b => b.INDVDL_PROV_ID, (a, b) => new { idvl = a, eml = b.Where(yy => yy.CNTCT_TYPE_CD == "EML").Select(x => x.CNTCT_TEXT).DefaultIfEmpty() })
                .SelectMany(a => a.eml.Select(b => new { idvl = a.idvl, eml = b }))
                //left outer join using provider id
                .GroupJoin(t.PROVs, a => a.idvl.PROV_ID, b => b.EPDS_NUM, (a, b) => new { idvl = a.idvl, eml = a.eml, cowname = b.Select(x => x.FULL_NAME).DefaultIfEmpty() })
                .SelectMany(a => a.cowname.Select(b => new { idvl = a.idvl, eml = a.eml, cowname = b }))
                //finally sort list
                .OrderBy(ii => ii.idvl.LAST_NAME)
                .ToList()
                /*
                 * finally, want to transform to a list of strings with some coherence in its ordinality
                 * which basically means the expected column data at a given index will be:
                 * prov_id, fname, lname, mid_initl, namesuffix, prov_credentials, ch_emp_flag, email, prov_status, full name on COW
                 * at indexes 0, 1, 2 ... etc
                */
                .Select(i => new string[]
                {
                    i.idvl.PROV_ID.ToString(),
                    i.idvl.LAST_NAME,
                    i.idvl.FIRST_NAME,
                    i.idvl.MID_INITL,
                    i.idvl.NAME_SUFFIX,
                    i.idvl.PROV_CREDENTIALS,
                    //i.idvl.CH_EMP_FLAG,
                    i.eml,
                    i.idvl.CH_EMP_ID.ToString(),
                    i.cowname,
                    i.idvl.PROV_STATUS = i.idvl.PROV_STATUS.Equals("A") ? "Active" : "Inactive"
                    
                }).ToArray();

            return rset;
        }
    }
}
