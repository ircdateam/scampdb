﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;

namespace ScampsDB.Query
{
    public static class Reports
    {
        private class XYrecord
        {
            public string x { get; set; }
            public string y { get; set; }
        }

        public static statData2D getHistoricEncounterCount(DateTime datelimit)
        {

            const string datefmt = "MM-dd-yyyy";
            const string HistEnQuery = "select to_char(clinic_appt,'yyyy-mm') as x, to_char(count(appt_id)) as y from pt_encounters where clinic_appt is not null and clinic_appt > to_date(:01,'mm-dd-yyyy') group by to_char(clinic_appt,'yyyy-mm') order by x desc";
            //var p = new SqParameter(){ ParameterName="datelimit", DbType=System.Data.DbType.Date,Value=datelimit};

            var ctx = SCAMPstore.Get();
            //var p = ctx.Connection.
            var result = ctx.ExecuteStoreQuery<XYrecord>(HistEnQuery, datelimit.ToString(datefmt)).ToList();
            //transpose 
            var data = new statData2D()
            {
                labels = result.Select(d => d.x).ToArray(),
                datapoints = result.Select(d => d.y).ToArray()
            };

            return data;
        }
    }

}
