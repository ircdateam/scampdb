﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;
using System.Data.Objects;

namespace ScampsDB.Query
{
    public static class Users
    {
        public static userProfileModel getUserProfile(string username)
        {
            if (string.IsNullOrEmpty(username))
                return null;
            var t = SCAMPstore.Get();
            var p = t.ADMIN_PROVIDER.Where(u => u.USER_NAME.ToLower() == username.ToLower())
                .Select(u => new userProfileModel()
                {
                    id = u.ID,
                    first_name = u.FIRST_NAME,
                    last_name = u.LAST_NAME,
                    email = u.EMAIL_ADDR,
                    scamps_prov_id = u.SCAMPS_PROV_ID,
                    hosp_prov_id = u.HOSP_PROV_ID,
                    title = u.TITLE_TXT,
                    authmode = u.AUTHENT_MODE
                }).FirstOrDefault();
            return p;
        }

        public static void pushAuthAudit(string userid, string machinename, string accesscode, string comment)
        {

            var t = SCAMPstore.Get();
            var audit = new AUDIT_TRAIL()
            {
                USER_ID = userid,
                ACCESS_CD = accesscode,
                MACHINE_NAME = machinename,
                CMNT_TXT = comment,
                AUDIT_DT_TM = DateTime.Now
            };
            t.AUDIT_TRAIL.AddObject(audit);
            t.SaveChanges();
            t.Refresh(RefreshMode.StoreWins, audit);
        }

        public static int authenticateUser(string username, string password, out int authmode)
        {
            //return codes
            // 0 - authenticated, 1 - user doesnt exist, 2 - user info populated but no data, 3 - invalid password, 4 - user is set to inactive/no_access
            authmode = -1;
            var t = SCAMPstore.Get();

            var provider = t.ADMIN_PROVIDER.FirstOrDefault(u => u.USER_NAME.ToLower() == username.ToLower());

            if (provider == null)
                return 1;
            if (!provider.ACTIVE_STATUS.Equals("Y", StringComparison.OrdinalIgnoreCase) || provider.PROFILE_NAME.Equals("NO_ACCESS", StringComparison.OrdinalIgnoreCase))
                return 4;
            authmode = provider.AUTHENT_MODE.HasValue ? provider.AUTHENT_MODE.Value : -1;

            if (authmode == -1 || string.IsNullOrEmpty(provider.PASSWORD_TXT))
                return 2;

            if (!BCrypt.Net.BCrypt.Verify(password, provider.PASSWORD_TXT))
                return 3;

            return 0;
        }

        public static bool userExists(string username)
        {
            var t = SCAMPstore.Get();
            bool exists = false;
            if (t.ADMIN_PROVIDER.Any(u => u.USER_NAME.ToUpper() == username.ToUpper()))
                exists = true;
            return exists;
        }

        public static bool updateUserProfile(string key, string identifier, int workfactor, string username, string password, string profilename, string fname, string lname, string email, short authmode)
        {
            var idcol = key.ToLower();
            var validKeys = new string[] { "id", "scamps_prov_id", "user_name", "hosp_prov_id" };

            if (!validKeys.Contains(idcol))
                return false;

            ADMIN_PROVIDER prof = null;
            var t = SCAMPstore.Get();

            int id = 0;
            try
            {
                switch (idcol)
                {
                    case "id":
                        if (!int.TryParse(idcol, out id))
                            break;
                        prof = t.ADMIN_PROVIDER.FirstOrDefault(p => p.ID == id);
                        break;
                    case "scamps_prov_id":
                        prof = t.ADMIN_PROVIDER.FirstOrDefault(p => p.HOSP_PROV_ID == idcol);
                        break;
                    case "user_name":
                        prof = t.ADMIN_PROVIDER.FirstOrDefault(p => p.USER_NAME.ToLower() == idcol);
                        break;
                    case "hosp_prov_id":
                        prof = t.ADMIN_PROVIDER.FirstOrDefault(p => p.HOSP_PROV_ID.ToLower() == idcol);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error looking up user: " + e.ToString());
                return false;
            }

            if (prof == null)
                return false;

            try
            {
                prof.PROFILE_NAME = profilename.ToUpper();
                prof.USER_NAME = username.ToUpper();
                prof.FIRST_NAME = fname;
                prof.LAST_NAME = lname;
                prof.PASSWORD_TXT = BCrypt.Net.BCrypt.HashPassword(password, workfactor);
                prof.AUTHENT_MODE = authmode;
                prof.EMAIL_ADDR = email;
                prof.FULL_NAME = string.Concat(fname, " ", lname);
                t.SaveChanges();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error updating user profile: " + e.ToString());
                return false;
            }
            return true;

        }
        public static bool createUserProfile(string username, string password, int workfactor, string profilename, string fname, string lname, string email, short authmode)
        {
            var t = SCAMPstore.Get();

            var prov = new ADMIN_PROVIDER()
            {
                USER_NAME = username.ToUpper(),
                PASSWORD_TXT = BCrypt.Net.BCrypt.HashPassword(password, workfactor),
                PROFILE_NAME = profilename.ToUpper(),
                FIRST_NAME = fname,
                AUTHENT_MODE = authmode,
                LAST_NAME = lname,
                FULL_NAME = string.Concat(fname, " ", lname),
                EMAIL_ADDR = email,
                ACTIVE_STATUS = "Y",

            };
            try
            {
                t.ADMIN_PROVIDER.AddObject(prov);
                t.SaveChanges();
                t.Refresh(RefreshMode.StoreWins, prov);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error inserting AD user's profile into database: {0}", e.ToString());
                return false;
            }

            return true;
        }

        public static bool updatePassword(string username, string password, int workfactor)
        {
            var t = SCAMPstore.Get();
            try
            {
                var up = t.ADMIN_PROVIDER.FirstOrDefault(u => u.USER_NAME.ToLower() == username.ToLower());
                if (up != null)
                {
                    up.PASSWORD_TXT = BCrypt.Net.BCrypt.HashPassword(password, workfactor);
                    t.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error updating password for user {0}: {1}", username, e.ToString());
            }
            return false;
        }

    }

}
