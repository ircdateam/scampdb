﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;
using ScampsDB.SearchModels;
using System.Data.Objects;
using ScampsDB.Extensions.String;

namespace ScampsDB.Query
{
    public static class Patients
    {
        private const string datemask = "MM/dd/yyyy";
        //private const string datetimemask = "MM&#47;dd&#47;yyyy hh:mm:ss tt";

        public static bool ValidMRN(string mrn)
        {
            //mrn can't be empty or exceed the max length for MRN allocated in database
            if (mrn.IsNullOrEmpty() || mrn.Length > 20)
                return false;
            var pt = SCAMPstore.Get();

            //if regex isn't defined, no restrictions
            if (!pt.ADMIN_CONFIG.Any(ac => ac.ATTRIB == "MRN_REGEX"))
                return true;

            const string _queryMRNRegex = "select 1 from admin_config where attrib = 'MRN_REGEX' and regexp_like(:01,attrib_val,'c')";

            var valid = pt.ExecuteStoreQuery<int?>(_queryMRNRegex, mrn).FirstOrDefault();

            if (valid.HasValue)
                return true;

            return false;
        }

        public static bool DoesPatientExist(string mrn)
        {
            var pt = SCAMPstore.Get();
            return pt.PT_MASTER.Any(t => t.MRN.ToUpper() == mrn.ToUpper());
        }

        public static PatientViewModel findPatient(string mrn)
        {
            if (string.IsNullOrEmpty(mrn))
                return null;
            var ptentity = SCAMPstore.Get();
            var pt = ptentity.PT_MASTER.FirstOrDefault(t => t.MRN.ToUpper().Equals(mrn.ToUpper()));

            if (pt == null)
                return null;

            var pat = new PatientViewModel()
            {
                mrn = pt.MRN,
                fname = pt.FNAME,
                lname = pt.LNAME,
                dob = pt.DOB,
                gender = pt.SEX,
                comment = pt.COMMENTS
            };

            return pat;
        }

        public static PatientViewModel getExternalPatient(string mrn)
        {
            if (string.IsNullOrEmpty(mrn))
                return null;
            var ptentity = new ExternalEntities();
            var pt = ptentity.MPIs.FirstOrDefault(t => t.MRN.ToUpper().Equals(mrn.ToUpper()));

            if (pt == null)
                return null;

            var pat = new PatientViewModel()
            {
                mrn = pt.MRN,
                fname = pt.PAT_DEMOGRAPH.FIRST_NAME,
                lname = pt.PAT_DEMOGRAPH.LAST_NAME,
                dob = pt.PAT_DEMOGRAPH.DOB,
                gender = pt.PAT_DEMOGRAPH.SEX_CD
            };

            return pat;
        }

        public static void insertPatient(PatientViewModel p)
        {
            var ErrorList = new string[] { 
                "MRN missing",
                "MRN already exists",
            };

            if (p == null || p.mrn.IsNullOrEmpty())
                throw new SCAMPException(ErrorList[0]);

            var ptentity = SCAMPstore.Get();

            if (ptentity.PT_MASTER.Any(pt => pt.MRN.ToUpper() == p.mrn.ToUpper()))
                throw new SCAMPException(ErrorList[1]);

            //set attributes for patient before insertion into db
            var pat = new PT_MASTER()
            {
                MRN = p.mrn.ToUpper(),
                FNAME = p.fname.ToUpper(),
                LNAME = p.lname.ToUpper(),
                DOB = p.dob,
                SEX = p.gender,
                COMMENTS = p.comment
            };

            ptentity.PT_MASTER.AddObject(pat);
            ptentity.SaveChanges();
            ptentity.Refresh(RefreshMode.StoreWins, pat);
        }

        public static void updatePatient(PatientViewModel p)
        {
            var ErrorList = new string[] { 
                "MRN missing",
                "Patient does not exist",
            };

            if (p.mrn.IsNullOrEmpty())
                throw new SCAMPException(ErrorList[0]);

            var ptentity = SCAMPstore.Get();

            var patient = ptentity.PT_MASTER.FirstOrDefault(pt => pt.MRN.ToUpper() == p.mrn.ToUpper());

            if (patient == null)
                throw new SCAMPException(ErrorList[1]);

            patient.FNAME = p.fname.ToUpper();
            patient.LNAME = p.lname.ToUpper();
            patient.DOB = p.dob;
            patient.SEX = p.gender;
            patient.COMMENTS = p.comment;
            ptentity.SaveChanges();
        }

        public static IQueryable<PAT_DEMOGRAPH> filterExternalFuzzy(PatientSearch pmodel)
        {
            var u = new ExternalEntities();
            var q = u.PAT_DEMOGRAPH.AsQueryable();

            if (!pmodel.firstname.IsNullOrEmpty())
                q = q.Where(p => p.FIRST_NAME.ToLower().Contains(pmodel.firstname.ToLower()));
            if (!pmodel.lastname.IsNullOrEmpty())
                q = q.Where(p => p.LAST_NAME.ToLower().Contains(pmodel.lastname.ToLower()));
            if (!pmodel.gender.IsNullOrEmpty())
                q = q.Where(p => p.SEX_CD.ToLower().Contains(pmodel.gender.ToLower()));
            if (!pmodel.MRN.IsNullOrEmpty())
                q = q.Where(p => p.MPI.MRN.ToLower().Contains(pmodel.MRN.ToLower()));

            return q;
        }
        public static IQueryable<PAT_DEMOGRAPH> filterExternalExact(PatientSearch pmodel)
        {
            var u = new ExternalEntities();
            var q = u.PAT_DEMOGRAPH.AsQueryable();

            if (!pmodel.firstname.IsNullOrEmpty())
                q = q.Where(p => p.FIRST_NAME.ToLower() == pmodel.firstname.ToLower());
            if (!pmodel.lastname.IsNullOrEmpty())
                q = q.Where(p => p.LAST_NAME.ToLower() == pmodel.lastname.ToLower());
            if (!pmodel.gender.IsNullOrEmpty())
                q = q.Where(p => p.SEX_CD.ToLower() == pmodel.gender.ToLower());
            if (!pmodel.MRN.IsNullOrEmpty())
                q = q.Where(p => p.MPI.MRN.ToLower() == pmodel.MRN.ToLower());

            return q;
        }
        public static IEnumerable<SearchResultModel> getExternalList(PatientSearch pmodel)
        {
            if (pmodel == null || !pmodel.hasParameters())
                return new SearchResultModel[0];

            var q = pmodel.exactSearch ? filterExternalExact(pmodel) : filterExternalFuzzy(pmodel);

            if (pmodel.DOB.HasValue)
                q = q.Where(p => p.DOB.HasValue && p.DOB.Value.Day == pmodel.DOB.Value.Day && p.DOB.Value.Month == pmodel.DOB.Value.Month && p.DOB.Value.Year == pmodel.DOB.Value.Year);

            if (pmodel.maxresults > 0)
                q = q.Take(pmodel.maxresults);

            return q.Select(pt => new SearchResultModel()
            {
                pdata = "icon-patExt",
                mrn = pt.MPI.MRN.ToUpper(),
                lname = pt.LAST_NAME,
                fname = pt.FIRST_NAME,
                dob = pt.DOB,
                gender = pt.SEX_CD
            });

        }


        public static IQueryable<PT_MASTER> filterListExact(ISCAMPEntity t, PatientSearch pmodel)
        {
            var q = t.PT_MASTER.AsQueryable();

            if (!pmodel.MRN.IsNullOrEmpty())
                q = q.Where(p => p.MRN.ToLower() == pmodel.MRN.ToLower());
            if (!pmodel.firstname.IsNullOrEmpty())
                q = q.Where(p => p.FNAME.ToLower() == pmodel.firstname.ToLower());
            if (!pmodel.lastname.IsNullOrEmpty())
                q = q.Where(p => p.LNAME.ToLower() == pmodel.lastname.ToLower());
            if (!pmodel.gender.IsNullOrEmpty())
                q = q.Where(p => p.SEX.ToLower() == pmodel.gender.ToLower());
            if (pmodel.DOB.HasValue)
                q = q.Where(p => p.DOB.HasValue && p.DOB.Value.Day == pmodel.DOB.Value.Day && p.DOB.Value.Month == pmodel.DOB.Value.Month && p.DOB.Value.Year == pmodel.DOB.Value.Year);

            return q;
        }

        public static IQueryable<PT_MASTER> filterListFuzzy(ISCAMPEntity t, PatientSearch pmodel)
        {
            var q = t.PT_MASTER.AsQueryable();

            if (!pmodel.MRN.IsNullOrEmpty())
                q = q.Where(p => p.MRN.ToLower().Contains(pmodel.MRN.ToLower()));
            if (!pmodel.firstname.IsNullOrEmpty())
                q = q.Where(p => p.FNAME.ToLower().Contains(pmodel.firstname.ToLower()));
            if (!pmodel.lastname.IsNullOrEmpty())
                q = q.Where(p => p.LNAME.ToLower().Contains(pmodel.lastname.ToLower()));
            if (!pmodel.gender.IsNullOrEmpty())
                q = q.Where(p => p.SEX.ToLower().Contains(pmodel.gender.ToLower()));
            if (pmodel.DOB.HasValue)
                q = q.Where(p => p.DOB.HasValue && p.DOB.Value.Day == pmodel.DOB.Value.Day && p.DOB.Value.Month == pmodel.DOB.Value.Month && p.DOB.Value.Year == pmodel.DOB.Value.Year);

            return q;
        }

        public static IEnumerable<SearchResultModel> getList(PatientSearch pmodel)
        {
            if (pmodel == null || !pmodel.hasParameters())
                return new SearchResultModel[0];

            var t = SCAMPstore.Get();
            IQueryable<PT_MASTER> q = pmodel.exactSearch ? filterListExact(t, pmodel) : filterListFuzzy(t, pmodel);

            if (pmodel.PID.HasValue)
                q = q.Where(pt => pt.ID == pmodel.PID.Value);
            //more complicated filters...
            if (pmodel.SCAMP.HasValue)
            {
                q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.SCAMP_ID == pmodel.SCAMP), a => a.MRN, b => b.MRN, (a, b) => a);
            }

            //disposition filter
            if (pmodel.disposition > 0)
            {
                switch (pmodel.disposition)
                {
                    case 1://TempExcluded
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == 0), a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL == 0);
                        break;
                    case 2://PermExcluded
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == -1), a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL == -1);
                        break;
                    case 3://Exited
                        //q = q.Where(en => en.STATUS_VAL == -2);
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == -2), a => a.MRN, b => b.MRN, (a, b) => a);
                        break;
                    case 4://EncounterWithin14
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL > 0), a => a.MRN, b => b.MRN, (a, b) => a)
                            .Join(t.PT_ENCOUNTERS
                                .Where(en => (en.CLINIC_APPT.HasValue && en.CLINIC_APPT > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -14)) ||
                                    (en.INPT_ADMIT.HasValue && en.INPT_ADMIT > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -14)) ||
                                    (en.EVENT_DT_TM.HasValue && en.EVENT_DT_TM > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -14))),
                                    a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL > 0 && en.CLINIC_APPT.HasValue && en.CLINIC_APPT.Value > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -14));
                        break;
                    case 5://included
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == 1), a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL == 1);
                        break;
                    case 6://toBeScreened
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == 2), a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL == 2);
                        break;
                    case 7://toBeRescreened
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == 3), a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL == 3);
                        break;
                    case 8://dischargedWithin7
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL > 0), a => a.MRN, b => b.MRN, (a, b) => a)
                            .Join(t.PT_ENCOUNTERS
                                .Where(en => en.INPT_DISCH.HasValue && en.INPT_DISCH > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -7))
                                , a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL > 0 && en.INPT_DISCH.HasValue && en.INPT_DISCH.Value > EntityFunctions.AddDays(EntityFunctions.TruncateTime(DateTime.Now), -7));
                        break;
                    case 9://IncludedFlagged
                        q = q.Where(p => p.FLAG_STATUS == -1).Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL == 1), a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL == 1 && en.FLAG_STATUS == -1);
                        break;
                    case 10://CurrentInpatients
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL > 0), a => a.MRN, b => b.MRN, (a, b) => a)
                            .Join(t.PT_ENCOUNTERS.Where(en => en.INPT_ADMIT.HasValue && !en.INPT_DISCH.HasValue && en.INPT_ADMIT.Value > EntityFunctions.AddYears(EntityFunctions.TruncateTime(DateTime.Now), -1)),
                            a => a.MRN, b => b.MRN, (a, b) => a);
                        //q = q.Where(en => en.STATUS_VAL > 0 && en.INPT_ADMIT.HasValue && en.INPT_ADMIT.Value > EntityFunctions.AddYears(EntityFunctions.TruncateTime(DateTime.Now), -1) && !en.INPT_DISCH.HasValue);
                        //enlist = enlist.Where(en => en.STATUS_VAL > 0 && en.INPT_ADMIT > DbFunctions.AddYears(DateTime.UtcNow,-1));
                        break;
                    case 11://CompletedWebForms
                        q = q.Join(t.PT_SCAMP_STATUS.Where(ss => ss.STATUS_VAL >= 0), a => a.MRN, b => b.MRN, (a, b) => a);

                        var tmpEn = q.Join(t.PT_ENCOUNTERS.Where(en => en.ENCOUNTER_STATUS == "51"), a => a.MRN, b => b.MRN, (a, b) => a);
                        var tmpIP = q.Join(t.PT_ENCOUNTERS.Join(t.VISITS_INPT_SUBUNIT.Where(ip => ip.ENCOUNTER_STATUS == "51"), c => c.HAR, d => d.HAR, (c, d) => c), a => a.MRN, b => b.MRN, (a, b) => a);
                        q = tmpEn.Union(tmpIP);
                        //q = q.Where(en => en.STATUS_VAL >= 0 && (en.ENC_STAT_ID == 51 || en.SUB_ENC_STAT.Equals("51")));
                        break;
                    default:
                        break;
                }
            }

            return q.Select(pt => new SearchResultModel()
            {
                pdata = "icon-patLocal",
                mrn = pt.MRN,
                lname = pt.LNAME,
                fname = pt.FNAME,
                dob = pt.DOB,
                gender = pt.SEX
            });
        }

    }

}
