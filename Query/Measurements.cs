﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;
using ScampsDB.ViewModels;

namespace ScampsDB.Query
{
    public static class Measurements
    {
        public static void getEncounterMeasurements(ref Dictionary<string, MeasurementViewModel[]> refDict, string appt_id, string sub_seq_val)
        {
            var currMeasurements = new Dictionary<string, List<MeasurementViewModel>>();


            var ctx = SCAMPstore.Get();

            IQueryable<MEASUREMENT> mquery = null;

            long apptid = 0;
            short subseq = 0;

            if (!long.TryParse(appt_id, out apptid))
                throw new SCAMPException("Invalid appointment id specified for measurement");


            if (!string.IsNullOrEmpty(sub_seq_val))
            {
                if (!short.TryParse(sub_seq_val, out subseq))
                    throw new SCAMPException("Invalid subunit sequence specified for measurement");

                mquery = ctx.MEASUREMENTS.Where(mm => mm.APPT_ID == apptid && mm.SUBUNIT_SEQ_VAL.HasValue && mm.SUBUNIT_SEQ_VAL == subseq);
            }
            else
            {
                mquery = ctx.MEASUREMENTS.Where(mm => mm.APPT_ID == apptid);
            }

            foreach (var m in mquery.OrderBy(mm => mm.ELEMENT_CD).OrderBy(mm => mm.SEQ_NUM))
            {
                var currkey = m.ELEMENT_CD.ToLower();

                if (!currMeasurements.ContainsKey(currkey))
                {
                    currMeasurements.Add(currkey, new List<MeasurementViewModel>());
                }
                currMeasurements[currkey].Add(new MeasurementViewModel(m.ID.ToString(), m.ELEMENT_CD.ToLower(), m.ELEMENT_VAL, (int)(m.SEQ_NUM.HasValue ? m.SEQ_NUM.Value : 1)));

            }

            refDict = currMeasurements.ToDictionary(kv => kv.Key, kv => kv.Value.ToArray());
        }

        public static void Insert(Dictionary<string, MeasurementViewModel[]> measurements, Dictionary<string, string> stub)
        {
            var isSubunit = stub.ContainsKey("subunit_seq_val") && !string.IsNullOrEmpty(stub["subunit_seq_val"]);

            var reqKeys = new string[] { "scamp_id", "appt_id", "har" };

            foreach (var k in reqKeys)
            {
                if (!stub.ContainsKey(k))
                    throw new SCAMPException("cannot insert measurement that is missing value for '" + k + "'");
            }

            var mquery = measurements.Where(kv => kv.Value != null)
                    .SelectMany(kv => kv.Value)
                    .Where(mm => mm != null);

            var M = new MEASUREMENT();

            int scamp_id = 0;
            long appt_id = 0;
            short subunit_seq_val = 0;

            if (!int.TryParse(stub["scamp_id"], out scamp_id))
                throw new SCAMPException("invalid scamp_id");
            if (!long.TryParse(stub["appt_id"], out appt_id))
                throw new SCAMPException("invalid appt_id");
            if (isSubunit && !short.TryParse(stub["subunit_seq_val"], out subunit_seq_val))
                throw new SCAMPException("invalid subunit_seq_val");

            var ctx = SCAMPstore.Get();

            foreach (var measure in mquery)
            {
                ctx.MEASUREMENTS.AddObject(new MEASUREMENT()
                {
                    SCAMP_ID = scamp_id,
                    APPT_ID = appt_id,
                    HAR = stub.ContainsKey("har") ? stub["har"] : "",
                    VISIT_TYPE = stub.ContainsKey("visit_type") ? stub["visit_type"] : "",
                    ELEMENT_CD = measure.ele_cd,
                    ELEMENT_VAL = measure.val,
                    SEQ_NUM = (short)measure.seq,
                    SUBUNIT_SEQ_VAL = (isSubunit ? (short?)subunit_seq_val : (short?)null),
                });
            }
            ctx.SaveChanges();
        }

        public static void Update(Dictionary<string, MeasurementViewModel[]> measurements)
        {
            //base collection for measurements with numeric IDs
            var mbase = measurements.SelectMany(kv => kv.Value).Where(mm => mm.id.All(ch => char.IsNumber(ch)));
            //create id list to query specific measurements to update
            var idlist = mbase.Select(mm => int.Parse(mm.id)).ToArray();
            //create a dictionary to lookup the measurements based on id
            var md = mbase.ToDictionary(mm => int.Parse(mm.id));

            var ctx = SCAMPstore.Get();

            foreach (var measure in ctx.MEASUREMENTS.Where(mm => idlist.Contains(mm.ID)))
            {

                if (!md.ContainsKey(measure.ID))
                    continue;
                var currm = md[measure.ID];

                measure.ELEMENT_VAL = currm.val;
                measure.SEQ_NUM = (short)currm.seq;
                measure.ELEMENT_CD = currm.ele_cd;
            }
            ctx.SaveChanges();
        }
    }

}
