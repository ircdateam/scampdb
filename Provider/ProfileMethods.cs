﻿using System;
using System.Collections.Generic;
using System.Linq;
using ScampsDB.Models;

namespace ScampsDB.Provider
{
    public static class classfindSizeExtension
    {

        public static int getSizeOf(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return 0;
            int sz = 0;
            foreach (var c in s)
                sz += sizeof(char);
            return sz;
        }
        public static int getSizeOf(this int i)
        {
            return sizeof(int);
        }
        public static int getSizeOf(this int? i)
        {
            return i.HasValue ? i.Value.getSizeOf() : 0;
        }
        public static int getSizeOf(this short i)
        {
            return sizeof(short);
        }
        public static int getSizeOf(this short? i)
        {
            return i.HasValue ? i.Value.getSizeOf() : 0;
        }
        public static int getSizeOf(this long i)
        {
            return sizeof(long);
        }
        public static int getSizeOf(this long? i)
        {
            return i.HasValue ? i.Value.getSizeOf() : 0;
        }
        public static int getSizeOf(this double i)
        {
            return sizeof(double);
        }
        public static int getSizeOf(this double? i)
        {
            return i.HasValue ? i.Value.getSizeOf() : 0;
        }
        public static int getSizeOf(this char c)
        {
            return sizeof(char);
        }
        public static int getSizeOf(this bool b)
        {
            return sizeof(bool);
        }
        public static int getSizeOf(this DateTime i)
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(i);
        }
        public static int getSizeOf(this DateTime? i)
        {
            return i.HasValue ? i.Value.getSizeOf() : 0;
        }
    }

    public class RoleMethods
    {
        public static bool IsUserInRole(string userName, string roleName)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(roleName))
                return false;
            bool inrole = false;
            try
            {
                var t = SCAMPstore.Get();
                inrole = t.ADMIN_PROVIDER.Any(u => u.USER_NAME.ToUpper() == userName.ToUpper()
                    && u.PROFILE_NAME.ToUpper() == roleName.ToUpper());
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("error checking user role membership: " + e.ToString());
            }
            return inrole;

        }

        public static string[] GetRolesForUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
                return new string[1];

            var roles = new string[1];

            try
            {
                var t = SCAMPstore.Get();
                var profile = t.ADMIN_PROVIDER.FirstOrDefault(u => u.USER_NAME.ToUpper() == userName.ToUpper());
                if (profile != null)
                    roles[0] = profile.PROFILE_NAME;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("error getting user roles: " + e.ToString());
            }
            return roles;
        }

        public static string[] GetAllRoles()
        {
            var roles = new string[1];
            try
            {
                var t = SCAMPstore.Get();
                roles = t.ADMIN_PROFILE.Select(p => p.PROFILE_NAME).ToArray();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("error getting role list: " + e.ToString());
            }

            return roles;
        }

        public static bool RoleExists(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
                return false;
            var exists = false;
            try
            {
                var t = SCAMPstore.Get();
                exists = t.ADMIN_PROFILE.Any(p => p.PROFILE_NAME.ToUpper() == roleName.ToUpper());
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("error checking role existence: " + e.ToString());
            }
            return exists;
        }

        public static string[] GetUsersInRole(string roleName)
        {
            var users = new string[1];
            if (string.IsNullOrEmpty(roleName))
                return users;

            try
            {
                var t = SCAMPstore.Get();
                users = t.ADMIN_PROVIDER.Where(p => p.PROFILE_NAME.ToUpper() == roleName.ToUpper())
                    .Select(p => p.USER_NAME).ToArray();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error getting user role membership: " + e.ToString());
            }
            return users;
        }
        /*
        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }
        
        public override string Name
        {
            get { return GetType().Name; }
        }

        public override String ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
         */
    }

    public class ProfileMethods
    {
        public static Dictionary<string, string> getProfile(string key, string value, string[] columns)
        {
            var d = new Dictionary<string, string>();
            columns.Distinct().ToList().ForEach(col => d.Add(col, ""));
            if (string.IsNullOrEmpty(key))
                return d;

            string lval = value.ToLower();
            int? pid = null;
            int tmp = 0;
            if (int.TryParse(value, out tmp))
                pid = tmp;


            var t = SCAMPstore.Get();
            var q = t.ADMIN_PROVIDER.AsQueryable();
            ADMIN_PROVIDER profile = null;

            switch (key.ToLower())
            {
                case "id":
                    if (pid.HasValue)
                        profile = q.FirstOrDefault(p => p.ID == pid.Value);
                    else
                        return d;
                    break;
                case "scamps_prov_id":
                    if (pid.HasValue)
                        profile = q.FirstOrDefault(p => p.SCAMPS_PROV_ID == pid.Value);
                    else
                        return d;
                    break;
                case "hosp_prov_id":
                    profile = q.FirstOrDefault(p => p.HOSP_PROV_ID.ToLower() == lval);
                    break;
                case "email_addr":
                    profile = q.FirstOrDefault(p => p.EMAIL_ADDR.ToLower() == lval);
                    break;
                case "user_name":
                    profile = q.FirstOrDefault(p => p.USER_NAME.ToLower() == lval);
                    break;
                default:
                    return d;
            }

            if (profile == null)
                return d;

            foreach (var c in columns)
            {
                switch (c.ToLower())
                {
                    case "id":
                        d["id"] = profile.ID.ToString();
                        break;
                    case "scamps_prov_id":
                        d["scamps_prov_id"] = profile.SCAMPS_PROV_ID.ToString();
                        break;
                    case "user_name":
                        d["user_name"] = profile.USER_NAME;
                        break;
                    case "email_addr":
                        d["email_addr"] = profile.EMAIL_ADDR;
                        break;
                    case "first_name":
                        d["first_name"] = profile.FIRST_NAME;
                        break;
                    case "last_name":
                        d["last_name"] = profile.LAST_NAME;
                        break;
                    case "title_txt":
                        d["title_txt"] = profile.TITLE_TXT;
                        break;
                    case "authent_mode":
                        d["authent_mode"] = profile.AUTHENT_MODE.HasValue ? profile.AUTHENT_MODE.Value.ToString() : "";
                        break;
                    case "password_txt":
                        d["password_txt"] = profile.PASSWORD_TXT;
                        break;
                    case "profile_name":
                        d["profile_name"] = profile.PROFILE_NAME;
                        break;
                    default:
                        break;
                }
            }
            return d;
        }
    }
}
