﻿using System;
using Newtonsoft.Json;

namespace ScampsDB.Extensions.Json
{
    public static class JsonTextWriterExtensions
    {
        public static JsonTextWriter WrtPropertyName(this JsonTextWriter w, string name)
        {
            w.WritePropertyName(name);
            return w;
        }
        public static JsonTextWriter WrtValue(this JsonTextWriter w, string value)
        {
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, string value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, bool value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, DateTime value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, short value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, short? value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, int value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtPropertyValue(this JsonTextWriter w, string name, long value)
        {
            w.WritePropertyName(name);
            w.WriteValue(value);
            return w;
        }
        public static JsonTextWriter WrtStartObject(this JsonTextWriter w)
        {
            w.WriteStartObject();
            return w;
        }
        public static JsonTextWriter WrtEndObject(this JsonTextWriter w)
        {
            w.WriteEndObject();
            return w;
        }
        public static JsonTextWriter WrtStartArray(this JsonTextWriter w)
        {
            w.WriteStartArray();
            return w;
        }
        public static JsonTextWriter WrtEndArray(this JsonTextWriter w)
        {
            w.WriteEndArray();
            return w;
        }
    }
    
}
