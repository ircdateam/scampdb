﻿namespace ScampsDB.Extensions.String
{
    static class StrExtension
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }
        public static string BetweenToken(this string str, char s, char e, int idx = 0)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            var start = str.IndexOf(s, idx);
            if (start == -1)
                return "";
            start++;
            var end = str.IndexOf(e, start);
            if (end == -1)
                return "";
            return str.Substring(start, end - start);
        }
    }
    
}
