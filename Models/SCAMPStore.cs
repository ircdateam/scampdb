﻿namespace ScampsDB.Models
{
    public class SCAMPstore
    {
        private static ISCAMPEntity datastore;
        private static bool IsInMemory = false;
        //NOTE: testSCAMPEntities is in-memory
        public static bool TestInit(bool IsInMemory = true)
        {
            SCAMPstore.IsInMemory = IsInMemory;
            return SCAMPstore.IsInMemory;
        }
        public static ISCAMPEntity Get()
        {
            if (IsInMemory)
            {
                if (datastore == null)
                    datastore = new testSCAMPEntities();
                return datastore;
            }
            else
                return new SCAMPSEntities();
        }
    }
}
