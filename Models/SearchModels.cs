﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScampsDB.SearchModels
{
    ///<summary>
    /// SearchModels in the defined classes below use their property definitions to filter query results
    /// For example, all the properties in PatientSearch (e.g. lastname, firstname) are definable search parameters
    /// if a property doesn't have a value set, it is not used as a search parameter.
    /// For example, if we instantiate a PatientSearch object and only set lastname and gender, those are the only
    /// two parameters used when querying for patients
    /// </summary>
    public class PatientSearch
    {
        public int? PID { get; set; }
        public string MRN { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public DateTime? DOB { get; set; }
        public string gender { get; set; }

        public int? SCAMP { get; set; }
        //disposition values
        /*
         * 0 - Nothing
         * 1 - Included
         * 2 - temporarily excluded
         * 3 - permanently excluded
         * 4 - Exited SCAMP
         * 5 - Encounter within 14 days
         * 6 - To be screened
         * 7 - To be rescreened
         * 8 - Current inpatients
         * 9 - Discharged within 7 days
         * 10 - Included (flagged)
         * 11 - Completed WebForms
         */
        public int disposition { get; set; }

        public bool exactSearch { get; set; }
        public bool scampDBonly { get; set; }
        public int maxresults { get; set; }

        public PatientSearch()
        {
        }

        public bool hasParameters()
        {
            bool hasP = false;

            hasP |= this.PID.HasValue || !string.IsNullOrEmpty(this.MRN) || !string.IsNullOrEmpty(this.lastname) || !string.IsNullOrEmpty(this.firstname) || !string.IsNullOrEmpty(this.gender);

            if (hasP) return true;

            hasP |= this.DOB.HasValue || this.SCAMP.HasValue || (disposition != 0);
            return hasP;
        }
    }

    //Encounter searches include all the fields that patient search requires
    public class EncounterSearch : PatientSearch
    {
        public string HAR { get; set; }
        public int? providerID { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
        public string visitType { get; set; }
        public string location { get; set; }
        /* status search filter flags: if bit is set, then status type is included
         * 1. COMPLETED
         * 2. IN_PROGRESS_INCOMPLETE
         * 3. IN_PROGRESS_NO_DATA
         * 4. IN_PROGRESS_FOR_REVIEW
         * 5. IN_PROGRESS_INCOMPLETE_SCANNED
         * 6. NOT_SCAMP_VISIT
         */
        public int? statusTypeFlag { get; set; }
        public EncounterSearch()
        {
            
        }

        //determines if any specific patient criteria has been specified
        public bool hasPatientQuery()
        {
            bool pinfo = false;

            pinfo |= this.PID.HasValue || !string.IsNullOrEmpty(this.HAR) || !string.IsNullOrEmpty(this.MRN) || !string.IsNullOrEmpty(this.lastname) || !string.IsNullOrEmpty(this.firstname) || !string.IsNullOrEmpty(this.gender);
            
            if (pinfo) return true;

            pinfo |= this.DOB.HasValue || this.SCAMP.HasValue || (disposition != 0);
            return pinfo;
        }

        public new bool hasParameters()
        {
            bool hasP = false;

            hasP |= hasPatientQuery();
            if (hasP) return true;

            hasP |= !string.IsNullOrEmpty(visitType) || !string.IsNullOrEmpty(location);
            if (hasP) return true;

            hasP |= providerID.HasValue || start.HasValue || end.HasValue || statusTypeFlag.HasValue;
            return hasP;
        }

        public PatientSearch getPatientSearchModel()
        {
            return new PatientSearch()
                       {
                           MRN = this.MRN,
                           lastname = this.lastname,
                           firstname = this.firstname,
                           DOB = this.DOB,
                           gender = this.gender,
                           SCAMP = this.SCAMP,
                           disposition = this.disposition,
                           exactSearch = this.exactSearch,
                           scampDBonly = this.scampDBonly
                       };
        }
    }
}
