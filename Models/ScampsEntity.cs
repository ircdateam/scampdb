﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Linq.Expressions;
namespace ScampsDB.Models
{
    public interface ISCAMPEntity
    {
        IObjectSet<SCAMP> SCAMPs { get; }
        IObjectSet<ADMIN_ENCOUNTERS> ADMIN_ENCOUNTERS { get; }
        IObjectSet<ADMIN_FORM_FIELDS> ADMIN_FORM_FIELDS { get; }
        IObjectSet<ADMIN_ELEMENT> ADMIN_ELEMENT { get; }
        IObjectSet<ADMIN_ELEMENT_LIST> ADMIN_ELEMENT_LIST { get; }

        IObjectSet<ADMIN_PROFILE> ADMIN_PROFILE { get; }
        IObjectSet<ADMIN_PROVIDER> ADMIN_PROVIDER { get; }
        IObjectSet<ADMIN_CONFIG> ADMIN_CONFIG { get; }
        IObjectSet<ADMIN_LOOKUP> ADMIN_LOOKUP { get; }
        IObjectSet<AUDIT_TRAIL> AUDIT_TRAIL { get; }

        IObjectSet<VISITS_INPT_SUBUNIT> VISITS_INPT_SUBUNIT { get; }
        IObjectSet<PT_MASTER> PT_MASTER { get; }
        IObjectSet<PT_ENCOUNTERS> PT_ENCOUNTERS { get; }
        IObjectSet<ENCOUNTER_LINK> ENCOUNTER_LINK { get; }
        IObjectSet<PT_SCAMP_STATUS> PT_SCAMP_STATUS { get; }
        IObjectSet<ADMIN_ENCOUNTER_STATUS> ADMIN_ENCOUNTER_STATUS { get; }
        IObjectSet<MEASUREMENT> MEASUREMENTS { get; }

        IObjectSet<ADMIN_SCREENING> ADMIN_SCREENING { get; }
        IObjectSet<ADMIN_SCAMPS_STATUS> ADMIN_SCAMPS_STATUS { get; }

        IObjectSet<PT_EN_LIST> PT_EN_LIST { get; }
        IObjectSet<EN_SUBUNITS_LIST> EN_SUBUNITS_LIST { get; }
        IObjectSet<VW_ENCOUNTER_SEARCH> VW_ENCOUNTER_SEARCH { get; }
        IObjectSet<VW_PT_CURR_ENROLL> VW_PT_CURR_ENROLL { get; }

        int ExecuteStoreCommand(string commandText, params object[] parameters);
        //ObjectResult<T> ExecuteStoreQuery<T>(string commandText, params object[] parameters);
        IEnumerable<T> ExecuteStoreQuery<T>(string commandText, params object[] parameters);
        int SaveChanges();
        void Refresh(RefreshMode mode, object Entity);

    }

    partial class SCAMPSEntities : ISCAMPEntity
    {
        public new IEnumerable<T> ExecuteStoreQuery<T>(string commandText, params object[] parameters)
        {
            return base.ExecuteStoreQuery<T>(commandText, parameters);
        }
    }

    public class testSCAMPEntities : ISCAMPEntity
    {

        /// <summary>
        /// ObjSetView acts as a placehoder for views which are derived from already existing in-memory data sets.
        /// type T is the target type of data object to provide (e.g. the 'VIEW')
        /// type U is the aggregated type which is to be transformed to the view.
        /// the basequery is used to store the base expression for the already-existing data.
        /// the transform function is called on each item in the basequery in order to provide the expected view data type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        public class ObjSetView<T, U> : IObjectSet<T> where T : class
        {
            public Func<U, T> transform { get; set; }
            public IQueryable<U> basequery { get; set; }
            public ObjSetView() { }

            public void AddObject(T entity) { throw new NotImplementedException(); }
            public void Attach(T entity) { throw new NotImplementedException(); }
            public void Detach(T entity) { throw new NotImplementedException(); }
            public void DeleteObject(T entity) { throw new NotImplementedException(); }

            public IEnumerator<T> GetEnumerator()
            {
                foreach (var i in basequery)
                {
                    var t = transform.Invoke(i);
                    yield return t;
                }
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                foreach (var i in basequery)
                {
                    var t = transform.Invoke(i);
                    yield return t;
                }
            }

            public Type ElementType
            {
                get { return typeof(T); }
            }

            public System.Linq.Expressions.Expression Expression
            {
                get {
                    return basequery.Select(i => transform.Invoke(i)).Expression;
                    //return basequery.Expression; 
                }
            }

            public IQueryProvider Provider
            {
                get {
                    //return basequery.Provider;
                    return basequery.Select(i => transform.Invoke(i)).Provider;
                }
            }
        }

        private class ObjSetList<T> : IObjectSet<T> where T : class{
            public List<T> list;
            Action<int, T> keyFunc;
            private int _ID = 0;
            public int ID { get {
                this._ID++;
                return this._ID; } }

            public ObjSetList(Action<int, T> keyFunc = null)
            {
                if (keyFunc != null)
                    this.keyFunc = keyFunc;
                list = new List<T>();
            }

            public void AddObject(T obj)
            {
                if (keyFunc != null)
                    keyFunc.Invoke(this.ID,obj);
                list.Add(obj);
            }
            public void DeleteObject(T obj)
            {
                list.Remove(obj);
            }
            public void Attach(T obj)
            {
                throw new NotImplementedException();
            }
            public void Detach(T obj)
            {
                throw new NotImplementedException();
            }
            
            public IEnumerable<T> GetEnumerator()
            {
                return list;
            }
            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                return list.GetEnumerator();
            }
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return list.GetEnumerator();
            }


            Expression IQueryable.Expression
            {
                get {
                    return list.AsQueryable().Expression;
                }
            }

            IQueryProvider IQueryable.Provider
            {
                get {
                    return list.AsQueryable().Provider;
                }
            }

            void IObjectSet<T>.AddObject(T entity)
            {
                if (keyFunc != null)
                    keyFunc.Invoke(this.ID, entity);
                list.Add(entity);
            }

            void IObjectSet<T>.Attach(T entity)
            {
                throw new NotImplementedException();
            }

            void IObjectSet<T>.DeleteObject(T entity)
            {
                list.Remove(entity);
            }

            void IObjectSet<T>.Detach(T entity)
            {
                throw new NotImplementedException();
            }

            Type IQueryable.ElementType
            {
                get
                {
                    return typeof(T);
                }
            }
        }

        private int _HAR = 200000;
        int HAR { get { return _HAR++; } }

        //lists
        ObjSetList<SCAMP> _SCAMPs = new ObjSetList<SCAMP>();
        ObjSetList<ADMIN_ENCOUNTERS> _ADMIN_ENCOUNTERS = new ObjSetList<ADMIN_ENCOUNTERS>();
        ObjSetList<ADMIN_FORM_FIELDS> _ADMIN_FORM_FIELDS = new ObjSetList<ADMIN_FORM_FIELDS>();
        ObjSetList<ADMIN_ELEMENT> _ADMIN_ELEMENT = new ObjSetList<ADMIN_ELEMENT>();
        ObjSetList<ADMIN_ELEMENT_LIST> _ADMIN_ELEMENT_LIST = new ObjSetList<ADMIN_ELEMENT_LIST>();

        ObjSetList<ADMIN_PROVIDER> _ADMIN_PROVIDER = new ObjSetList<ADMIN_PROVIDER>((a, b) => { b.ID = a; b.SCAMPS_PROV_ID = a + 1000; });
        ObjSetList<ADMIN_PROFILE> _ADMIN_PROFILE = new ObjSetList<ADMIN_PROFILE>((a, b) => b.ID = a);
        ObjSetList<ADMIN_CONFIG> _ADMIN_CONFIG = new ObjSetList<ADMIN_CONFIG>();
        ObjSetList<ADMIN_LOOKUP> _ADMIN_LOOKUP = new ObjSetList<ADMIN_LOOKUP>();
        ObjSetList<AUDIT_TRAIL> _AUDIT_TRAIL = new ObjSetList<AUDIT_TRAIL>((a, b) => b.ID = a);
        
        ObjSetList<VISITS_INPT_SUBUNIT> _VISITS_INPT_SUBUNIT = new ObjSetList<VISITS_INPT_SUBUNIT>((a,b) => b.ID=a);
        ObjSetList<PT_MASTER> _PT_MASTER = new ObjSetList<PT_MASTER>((a, b) => b.ID = a);
        ObjSetList<PT_ENCOUNTERS> _PT_ENCOUNTERS = new ObjSetList<PT_ENCOUNTERS>((a, b) => { b.ID = a; b.APPT_ID = a; });
        ObjSetList<ENCOUNTER_LINK> _ENCOUNTER_LINK = new ObjSetList<ENCOUNTER_LINK>();
        ObjSetList<PT_SCAMP_STATUS> _PT_SCAMP_STATUS = new ObjSetList<PT_SCAMP_STATUS>();
        ObjSetList<ADMIN_ENCOUNTER_STATUS> _ADMIN_ENCOUNTER_STATUS = new ObjSetList<ADMIN_ENCOUNTER_STATUS>();
        ObjSetList<MEASUREMENT> _MEASUREMENTS = new ObjSetList<MEASUREMENT>((a,b) => b.ID = a);

        ObjSetList<ADMIN_SCREENING> _ADMIN_SCREENING = new ObjSetList<ADMIN_SCREENING>((a, b) => b.ID = a);
        ObjSetList<ADMIN_SCAMPS_STATUS> _ADMIN_SCAMPS_STATUS = new ObjSetList<ADMIN_SCAMPS_STATUS>((a, b) => b.ID = a);

        ObjSetList<VW_ENCOUNTER_SEARCH> _VW_ENCOUNTER_SEARCH = new ObjSetList<VW_ENCOUNTER_SEARCH>();
        ObjSetView<EN_SUBUNITS_LIST, Tuple<PT_ENCOUNTERS, VISITS_INPT_SUBUNIT>> _EN_SUBUNITS_LIST = new ObjSetView<EN_SUBUNITS_LIST, Tuple<PT_ENCOUNTERS, VISITS_INPT_SUBUNIT>>();
        ObjSetView<PT_EN_LIST, Tuple<PT_ENCOUNTERS, ADMIN_ENCOUNTERS, ADMIN_ENCOUNTER_STATUS>> _PT_EN_LIST = new ObjSetView<PT_EN_LIST, Tuple<PT_ENCOUNTERS, ADMIN_ENCOUNTERS, ADMIN_ENCOUNTER_STATUS>>();
        ObjSetView<VW_PT_CURR_ENROLL, Tuple<SCAMP, ADMIN_SCREENING>> _VW_PT_CURR_ENROLL = new ObjSetView<VW_PT_CURR_ENROLL, Tuple<SCAMP, ADMIN_SCREENING>>();

        /// <summary>
        /// does a base load of data. This includes data for tables like:
        /// ADMIN_LOOKUP
        /// ADMIN_CONFIG
        /// ADMIN_PROFILE
        /// ADMIN_PROVIDER
        /// SCAMP
        /// ADMIN_SCAMP_STATUS
        /// ADMIN_ENCOUNTER_STATUS
        /// </summary>
        public void baseLoad()
        {
            var config_attribs = new string[] {
                "SITENAME","LANGUAGE_CODE","CODE_VERSION","SITECODE","CSEED","MANUAL_HAR_SEQ_VAL","MANUAL_EVENT_ID_SEQ_VAL",
                "SCHEMA_VERSION","DATEFORMAT"
            };
            var attribs = new string[]{
                "IRCDA TEST SITE","EN","2.9.b","IRCDATEST","alc22CUQTpl0hq+HpcaYNW0+6OMZ0v6Zcr7c/EsbsrJP2aWLicefuSZw6PBznFwcDZ5M/JRzbCiDt0QDVSQ70g==",
                "100000","100000000","1.07","yy/mm/dd"
            };

            //populate admin_config
            foreach (var i in Enumerable.Range(0, config_attribs.Length))
            {
                _ADMIN_CONFIG.list.Add(new ADMIN_CONFIG() { ID = _ADMIN_CONFIG.ID, ATTRIB = config_attribs[i], ATTRIB_VAL = attribs[i] });
            }

            //populate admin_profile
            _ADMIN_PROFILE.AddObject(new ADMIN_PROFILE() { ID = _ADMIN_PROFILE.ID, PROFILE_NAME = "SUPER_PROVIDER", ADMIN_USER = "Y", LOGIN = "Y", EDIT_ALL = "N", DATA_COORD = "N", PROVIDER = "Y" });
            _ADMIN_PROFILE.AddObject(new ADMIN_PROFILE() { ID = _ADMIN_PROFILE.ID, PROFILE_NAME = "DC", ADMIN_USER = "Y", LOGIN = "Y", EDIT_ALL = "Y", DATA_COORD = "Y", PROVIDER = "N" });
            _ADMIN_PROFILE.AddObject(new ADMIN_PROFILE() { ID = _ADMIN_PROFILE.ID, PROFILE_NAME = "SUPER_USER", ADMIN_USER = "Y", LOGIN = "Y", EDIT_ALL = "Y", DATA_COORD = "Y", PROVIDER = "Y" });
            _ADMIN_PROFILE.AddObject(new ADMIN_PROFILE() { ID = _ADMIN_PROFILE.ID, PROFILE_NAME = "NO_ACCESS", ADMIN_USER = "N", LOGIN = "N", EDIT_ALL = "N", DATA_COORD = "N", PROVIDER = "N" });
            _ADMIN_PROFILE.AddObject(new ADMIN_PROFILE() { ID = _ADMIN_PROFILE.ID, PROFILE_NAME = "PROVIDER", ADMIN_USER = "N", LOGIN = "Y", EDIT_ALL = "N", DATA_COORD = "N", PROVIDER = "Y" });

            //populate admin_providers
            _ADMIN_PROVIDER.AddObject(new ADMIN_PROVIDER()
            {
                ID = _ADMIN_PROVIDER.ID,
                SCAMPS_PROV_ID = _ADMIN_PROVIDER.ID,
                EMAIL_ADDR = "test@test.org",
                FIRST_NAME = "Test",
                LAST_NAME = "Provider",
                FULL_NAME = "Test Provider, Jr.",
                AUTHENT_MODE = 1,
                USER_NAME = "test",
                PASSWORD_TXT = "$2a$12$pNmRAGt50NZDe9ohIAGy2utO1cn3nKc84KcE6htSyYiCix2QgCoWG",
                ACTIVE_STATUS = "Y",
                PROFILE_NAME = "SUPER_USER",
                INCEPT_DTM = DateTime.Now,
                UPDATE_DTM = DateTime.Now
            });

            //populate scamps
            _SCAMPs.AddObject(new SCAMP()
            {
                SCAMP_ID = _SCAMPs.ID,
                SCAMP_CD = "TEST",
                SCAMP_NAME = "TEST",
                ALLOW_TEMP_EXCLUDE = 0,
                DISPLAY_NAME = "TEST SCAMP",
                INCEPT_DTM = DateTime.Now,
                UPDATE_DTM = DateTime.Now
            });

            //add encounter for scamp (TEST ECHO)
            _ADMIN_ENCOUNTERS.AddObject(new ADMIN_ENCOUNTERS()
            {
                ENCOUNTER_ID = _ADMIN_ENCOUNTERS.ID,
                ENCOUNTER_NAME = "TEST ECHO",
                ENCOUNTER_NAME_SHORT = "TEST ECHO",
                FORM_NAME = "TEST_ECHO",
                WEB_FORM_APPROVED = "Y",
                INCEPT_DTM = DateTime.Now,
                SCAMP_ID = _SCAMPs.First().SCAMP_ID,
                SEQ_NUM = 1,
                UPDATE_DTM = DateTime.Now,
                VISIBLE_FLAG = "Y"
            });

            //initalize all registered SCAMPs with statuses
            initSCAMPstatus();

            var locs = new string[] { "Inpatient clinic", "Outpatient clinic", "Event clinic" };
            var lcode = new string[] { "LOCTN_INPT", "LOCTN_OUTPT", "LOCTN_EVENT" };
            var levents = new string[] { "CLINIC", "DEATH", "ECHO", "EXERCISE", "LABS", "MRI", "SURG" };

            //populate admin_lookup with I/O/E locations, events
            foreach (var i in Enumerable.Range(0, locs.Length))
            {
                _ADMIN_LOOKUP.AddObject(new ADMIN_LOOKUP()
                {
                    ID = _ADMIN_LOOKUP.ID,
                    VISIBLE_ABBR = "Y",
                    UPDATE_DTM = DateTime.Now,
                    INCEPT_DTM = DateTime.Now,
                    LOOKUP_CD = lcode[i],
                    DESCRIPTION_TXT = locs[i],
                    VALUE_TXT = locs[i]
                });
            }
            foreach (var i in Enumerable.Range(0, levents.Length))
            {
                _ADMIN_LOOKUP.AddObject(new ADMIN_LOOKUP()
                {
                    ID = _ADMIN_LOOKUP.ID,
                    VISIBLE_ABBR = "Y",
                    UPDATE_DTM = DateTime.Now,
                    INCEPT_DTM = DateTime.Now,
                    LOOKUP_CD = "EVENT_TYPE",
                    DESCRIPTION_TXT = levents[i],
                    VALUE_TXT = levents[i]
                });
            }
            //define encounter statuses
            var enc_stat = new Tuple<short, string, short, string>[]{
                new Tuple<short, string, short, string>(1,"For provider: Packet delivered, form to be completed",2,"IN_PROGRESS_NO_DATA"),
                new Tuple<short, string, short, string>(3,"For DC: Completed, SDF entered",5,"COMPLETED"),
                new Tuple<short, string, short, string>(7,"For DC: Form returned by provider, data to be entered",4,"IN_PROGRESS_FOR_REVIEW"),
                new Tuple<short, string, short, string>(8,"For DC: Screened",1,"IN_PROGRESS_NO_DATA"),
                new Tuple<short, string, short, string>(10,"For DC: Non-SCAMPs visit (no data to enter)",6,"NOT_SCAMP_VISIT"),
                new Tuple<short, string, short, string>(11,"For DC: No Show / Cancelled Appt",7,"NOT_SCAMP_VISIT"),
                new Tuple<short, string, short, string>(15,"For DC: Missed visit, SCAMP eligible but form not sent in time",9,"NOT_SCAMP_VISIT"),
                new Tuple<short, string, short, string>(18,"For DC: Missed visit, SCAMP eligible but provider never returned form",10,"NOT_SCAMP_VISIT"),
                new Tuple<short, string, short, string>(20,"For provider: Form returned but incomplete, needs follow-up",3,"IN_PROGRESS_INCOMPLETE_SCANNED"),
                new Tuple<short, string, short, string>(22,"For DC: Provider deemed SCAMP ineligible visit",8,"NOT_SCAMP_VISIT"),
                new Tuple<short, string, short, string>(50,"For provider: Form returned but incomplete, needs follow-up (email URL link)",11,"IN_PROGRESS_INCOMPLETE"),
                new Tuple<short, string, short, string>(51,"For DC: Web form to be reviewed, completed by provider",13,"IN_PROGRESS_FOR_REVIEW"),
                new Tuple<short, string, short, string>(52,"For provider: Form to be completed (email URL link)",12,"IN_PROGRESS_NO_DATA"),
                new Tuple<short, string, short, string>(98,"For DC: Event Completed",14,"COMPLETED"),
                new Tuple<short, string, short, string>(99,"(Null)",15,"NOT_SCAMP_VISIT")
            };
            //base load encounter statuses
            foreach (var e in enc_stat)
            {
                _ADMIN_ENCOUNTER_STATUS.AddObject(new ADMIN_ENCOUNTER_STATUS()
                {
                    STATUS_ID = e.Item1,
                    STATUS_TEXT = e.Item2,
                    SEQ_NUM = e.Item3,
                    STATUS_TYPE = e.Item4,
                    INCEPT_DTM = DateTime.Now,
                    UPDATE_DTM = DateTime.Now
                });
            }

            //add a test patient
            _PT_MASTER.AddObject(new PT_MASTER()
            {
                MRN = "XX123",
                FNAME = "John",
                LNAME = "Dough",
                ID = _PT_MASTER.ID,
                COMMENTS = "Softy",
                SEX = "M",
                UPDATE_DTM = DateTime.Now,
                INCEPT_DTM = DateTime.Now,
                DOB = new DateTime(1990, 1, 2)
            });

            var testencounterform = _ADMIN_ENCOUNTERS.First();
            //var testihar = string.Concat("XX", this.HAR);
            //add an outpatient test encounter for test patient
            var testoen = new PT_ENCOUNTERS()
            {
                MRN="XX123",ENCOUNTER_CD="O", HAR = string.Concat("XX",this.HAR),
                CLINIC_APPT = DateTime.Now.AddMonths(-1),
                ENCOUNTER_TYPE = testencounterform.ENCOUNTER_ID,
                INCEPT_DTM = DateTime.Now, UPDATE_DTM = DateTime.Now, UPDT_DT_TM = DateTime.Now
            };
            _PT_ENCOUNTERS.AddObject(testoen);

            //add event test encounter for test patient
            //add inpatient test encounter for test patient

            var testien = new PT_ENCOUNTERS()
            {
                MRN = "XX123",
                ENCOUNTER_CD = "I",
                HAR = "FIN001",
                INPT_ADMIT = DateTime.Now.AddMonths(-1),
                INCEPT_DTM = DateTime.Now,UPDATE_DTM = DateTime.Now,UPDT_DT_TM = DateTime.Now
            };

            var testsub = new VISITS_INPT_SUBUNIT()
            {
                SCAMP_ID = testencounterform.SCAMP_ID,
                HAR = "FIN001",
                SEQ_VAL = 1,
                DATE_DT = DateTime.Now.AddDays(-5),
            };

            _PT_ENCOUNTERS.AddObject(testien);
            _VISITS_INPT_SUBUNIT.AddObject(testsub);

            //measurements for test outpatient encounter
            var testm_outpat = new MEASUREMENT[]{
                new MEASUREMENT(){ ELEMENT_CD="TEST_TXT",ELEMENT_VAL="TEXT_123", SEQ_NUM = 1 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="1", SEQ_NUM = 1 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="2", SEQ_NUM = 2 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="5", SEQ_NUM = 3 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="7", SEQ_NUM = -1 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIB",ELEMENT_VAL="-1", SEQ_NUM = -1 }
            };
            foreach (var mm in testm_outpat)
            {
                mm.APPT_ID = testoen.APPT_ID;
                _MEASUREMENTS.AddObject(mm);
            }
            //measurements for test subunit encounter
            var testm_inpat = new MEASUREMENT[]{
                new MEASUREMENT(){ ELEMENT_CD="TEST_TXT",ELEMENT_VAL="TEXT_123", SEQ_NUM = 1 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="1", SEQ_NUM = 1 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="2", SEQ_NUM = 2 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="5", SEQ_NUM = 3 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIA",ELEMENT_VAL="7", SEQ_NUM = -1 },
                new MEASUREMENT(){ ELEMENT_CD="TEST_MULTIB",ELEMENT_VAL="-1", SEQ_NUM = -1 }
            };
            foreach (var mm in testm_inpat)
            {
                mm.APPT_ID = testien.APPT_ID;
                mm.HAR = testien.HAR;
                mm.SUBUNIT_SEQ_VAL = testsub.SEQ_VAL;
                _MEASUREMENTS.AddObject(mm);
            }
            
        }

        /// <summary>
        /// creates statuses for registered SCAMPs that have status definitions missing
        /// </summary>
        public void initSCAMPstatus()
        {
            string[] stat_txt = new string[] { "Excluded (permanent)", "Excluded (temporary)", "Exited from SCAMP", "Included", "To be Rescreened", "To be screened" };
            short[] stat_val = new short[] { -1, 0, -2, 1, 3, 2 };


            var candidates = _SCAMPs.list.Where(sc => !_ADMIN_SCAMPS_STATUS.Any(ss => ss.ID == sc.SCAMP_ID));

            //for each scamp, create status codes
            foreach (var s in candidates)
            {
                foreach (var i in Enumerable.Range(0, stat_val.Length))
                    _ADMIN_SCAMPS_STATUS.AddObject(new ADMIN_SCAMPS_STATUS()
                    {
                        ID = _ADMIN_SCAMPS_STATUS.ID,
                        SCAMP_ID = s.SCAMP_ID,
                        SCAMP = s,
                        STATUS_TXT = stat_txt[i],
                        STATUS_VAL = stat_val[i]
                    });
            }
        }

        public testSCAMPEntities()
        {
        //begin defining view expressions

            //view for a patient's currently enrolled scamps (VW_PT_CURR_ENROLL)
            var currenrollquery = _SCAMPs.GroupJoin(
                _ADMIN_SCREENING.GroupBy(sc => sc.MRN)
                .Select(sc => sc.GroupBy(se => se.SCAMP_ID))
                .SelectMany(sc => sc.Select(se => se.First(ti => ti.AUDIT_DT_TM == se.Max(sei => sei.AUDIT_DT_TM)))),
                a => a.SCAMP_ID,
                b => b.SCAMP_ID,
                (a, b) => new Tuple<SCAMP, ADMIN_SCREENING>(a, b.FirstOrDefault())).AsQueryable();

            Func<Tuple<SCAMP, ADMIN_SCREENING>, VW_PT_CURR_ENROLL> enrolltx = a =>
                new VW_PT_CURR_ENROLL()
                {
                    AUDIT_DT_TM = (a.Item2 == null ? null : a.Item2.AUDIT_DT_TM),
                    SCAMP_ID = a.Item1.SCAMP_ID,
                    USER_ID = (a.Item2 == null ? "" : a.Item2.USER_ID),
                    STATUS_VAL = (a.Item2 == null ? null : (short?)short.Parse(a.Item2.NEW_VAL)),
                    SCAMP_NAME = a.Item1.SCAMP_NAME,
                    MRN = (a.Item2 == null ? "" : a.Item2.MRN),
                    CMNT_TXT = (a.Item2 == null ? "" : a.Item2.CMNT_TXT)
                };

            _VW_PT_CURR_ENROLL.basequery = currenrollquery;
            _VW_PT_CURR_ENROLL.transform = enrolltx;

            //view for a patient's encounters on the patient detail page (PT_EN_LIST)
            var patencquery = _PT_ENCOUNTERS.Select(en => new Tuple<PT_ENCOUNTERS, ADMIN_ENCOUNTERS, ADMIN_ENCOUNTER_STATUS>(
                    en,_ADMIN_ENCOUNTERS.FirstOrDefault(i => i.ENCOUNTER_ID == en.ENCOUNTER_TYPE), string.IsNullOrEmpty(en.APPT_STATUS) ? null : _ADMIN_ENCOUNTER_STATUS.FirstOrDefault(i => i.STATUS_ID == short.Parse(en.APPT_STATUS)))
                );
            Func<Tuple<PT_ENCOUNTERS, ADMIN_ENCOUNTERS, ADMIN_ENCOUNTER_STATUS>, PT_EN_LIST> patentx = a =>
                new PT_EN_LIST()
                {
                    MRN=a.Item1.MRN,APPT_ID=a.Item1.APPT_ID,HAR=a.Item1.HAR,APPT_STATUS=a.Item1.APPT_STATUS,PROVIDERNAME= ( a.Item1.SCAMPS_PROV_ID.HasValue ? _ADMIN_PROVIDER.FirstOrDefault(pr => pr.SCAMPS_PROV_ID == a.Item1.SCAMPS_PROV_ID).FULL_NAME : "") ,
                    ENCOUNTERDATE = (a.Item1.CLINIC_APPT ?? a.Item1.INPT_ADMIT ?? a.Item1.EVENT_DT_TM ?? DateTime.MinValue),
                    DISCHARGEDATE=a.Item1.INPT_DISCH,ENCOUNTER_CD=a.Item1.ENCOUNTER_CD,
                    VTYPE = a.Item1.ENCOUNTER_CD == "E" ? (a.Item1.EVENT_TYPE_ABBR + " EVENT") : a.Item1.ENCOUNTER_CD == "O" ? "OUTPATIENT" : "INPATIENT",
                    AGEMINUTE = (decimal?)(a.Item1.CLINIC_APPT ?? a.Item1.INPT_ADMIT ?? a.Item1.EVENT_DT_TM ?? DateTime.MinValue).Subtract(_PT_MASTER.First(pt => pt.MRN.Equals(a.Item1.MRN, StringComparison.OrdinalIgnoreCase)).DOB ?? DateTime.MinValue).TotalMinutes,
                    LOCATION = a.Item1.CLINIC_NAME ?? a.Item1.INPT_FLOOR ?? a.Item1.LOCTN_ABBR,
                    SCAMPNAME = a.Item2 == null ? "" : _SCAMPs.FirstOrDefault(sc => sc.SCAMP_ID == a.Item2.SCAMP_ID).SCAMP_NAME,
                    ENCOUNTERNAME = a.Item2 == null ? "" : string.IsNullOrEmpty(a.Item2.ENCOUNTER_NAME) ? "" : "["+_SCAMPs.FirstOrDefault(sc=>sc.SCAMP_ID==a.Item2.SCAMP_ID).SCAMP_NAME+"] "+a.Item2.ENCOUNTER_NAME,
                    FORM_NAME = a.Item2 == null ? "" : a.Item2.FORM_NAME,
                    ENCOUNTER_URL = a.Item2 == null ? "" : a.Item2.ENCOUNTER_URL,
                    WEB_FORM_APPROVED = a.Item2 == null ? 0 : a.Item2.WEB_FORM_APPROVED == "Y" ? 1 : 0,
                    STATUSTEXT = a.Item3 == null ? "" : a.Item3.STATUS_TEXT,
                    STATUSTYPE = a.Item3 == null ? "" : a.Item3.STATUS_TYPE,
                    LINK_ID = _ENCOUNTER_LINK.Any(el => el.SOURCE_ID == a.Item1.APPT_ID) ? (long?)a.Item1.APPT_ID : null,
                    SUB_ID = _VISITS_INPT_SUBUNIT.Any(sv => sv.HAR == a.Item1.HAR) ? (decimal?)_VISITS_INPT_SUBUNIT.FirstOrDefault(sv => sv.HAR == a.Item1.HAR).ID : null
                };
            _PT_EN_LIST.basequery = patencquery;
            _PT_EN_LIST.transform = patentx;

            //view for listing subunit encounters (EN_SUBUNITS_LIST)
            var ensubquery = _VISITS_INPT_SUBUNIT.Join(_PT_ENCOUNTERS, a => a.HAR, b => b.HAR, (sub, en) =>
                new Tuple<PT_ENCOUNTERS, VISITS_INPT_SUBUNIT>(en,sub));

            Func<Tuple<PT_ENCOUNTERS, VISITS_INPT_SUBUNIT>, EN_SUBUNITS_LIST> ensubtx = a =>
                new EN_SUBUNITS_LIST()
                {
                    SUB_ID = a.Item2.ID,APPT_ID = a.Item1.APPT_ID,HAR = a.Item2.HAR,DATE_DT=a.Item2.DATE_DT,
                    ATTENDING=a.Item2.ATTENDING,
                    SCAMPNAME=a.Item2.ENCOUNTER_TYPE.HasValue ? _SCAMPs.FirstOrDefault(sc => sc.SCAMP_ID == _ADMIN_ENCOUNTERS.FirstOrDefault(ae => ae.ENCOUNTER_ID == a.Item2.ENCOUNTER_TYPE).SCAMP_ID).SCAMP_NAME : "",
                    ENCOUNTERNAME=a.Item2.ENCOUNTER_TYPE.HasValue ? "["+_SCAMPs.FirstOrDefault(sc => sc.SCAMP_ID == _ADMIN_ENCOUNTERS.FirstOrDefault(ae => ae.ENCOUNTER_ID == a.Item2.ENCOUNTER_TYPE).SCAMP_ID).SCAMP_NAME+"] "+ _ADMIN_ENCOUNTERS.FirstOrDefault(ae => ae.ENCOUNTER_ID == a.Item2.ENCOUNTER_TYPE).ENCOUNTER_NAME : "",
                    FORM_NAME=a.Item2.ENCOUNTER_TYPE.HasValue ? _ADMIN_ENCOUNTERS.FirstOrDefault(ae=>ae.ENCOUNTER_ID==a.Item2.ENCOUNTER_TYPE).FORM_NAME : "",
                    ENCOUNTER_URL=a.Item2.ENCOUNTER_TYPE.HasValue ? _ADMIN_ENCOUNTERS.FirstOrDefault(ae=>ae.ENCOUNTER_ID==a.Item2.ENCOUNTER_TYPE).ENCOUNTER_URL : "",
                    WEB_FORM_APPROVED=a.Item2.ENCOUNTER_TYPE.HasValue ? _ADMIN_ENCOUNTERS.FirstOrDefault(ae=>ae.ENCOUNTER_ID==a.Item2.ENCOUNTER_TYPE).WEB_FORM_APPROVED == "Y" ? 1 : 0 : 0,
                    STATUSTEXT = string.IsNullOrEmpty(a.Item2.ENCOUNTER_STATUS) ? "" : _ADMIN_ENCOUNTER_STATUS.FirstOrDefault(es => es.STATUS_ID == short.Parse(a.Item2.ENCOUNTER_STATUS)).STATUS_TEXT,
                    STATUSTYPE = string.IsNullOrEmpty(a.Item2.ENCOUNTER_STATUS) ? "" : _ADMIN_ENCOUNTER_STATUS.FirstOrDefault(es => es.STATUS_ID == short.Parse(a.Item2.ENCOUNTER_STATUS)).STATUS_TYPE
                };
            _EN_SUBUNITS_LIST.basequery = ensubquery;
            _EN_SUBUNITS_LIST.transform = ensubtx;
        }

        #region objsetAccessors
        public IObjectSet<SCAMP> SCAMPs
        {
            get { return this._SCAMPs; }
        }

        public IObjectSet<ADMIN_ENCOUNTERS> ADMIN_ENCOUNTERS
        {
            get { return _ADMIN_ENCOUNTERS; }
        }

        public IObjectSet<ADMIN_FORM_FIELDS> ADMIN_FORM_FIELDS
        {
            get { return _ADMIN_FORM_FIELDS; }
        }

        public IObjectSet<ADMIN_ELEMENT> ADMIN_ELEMENT
        {
            get { return _ADMIN_ELEMENT; }
        }

        public IObjectSet<ADMIN_ELEMENT_LIST> ADMIN_ELEMENT_LIST
        {
            get { return _ADMIN_ELEMENT_LIST; }
        }

        public IObjectSet<ADMIN_PROFILE> ADMIN_PROFILE
        {
            get { return _ADMIN_PROFILE; }
        }

        public IObjectSet<ADMIN_PROVIDER> ADMIN_PROVIDER
        {
            get { return _ADMIN_PROVIDER; }
        }

        public IObjectSet<AUDIT_TRAIL> AUDIT_TRAIL
        {
            get { return _AUDIT_TRAIL; }
        }

        public IObjectSet<ADMIN_CONFIG> ADMIN_CONFIG
        {
            get { return _ADMIN_CONFIG; }
        }

        public IObjectSet<ADMIN_LOOKUP> ADMIN_LOOKUP
        {
            get { return _ADMIN_LOOKUP; }
        }

        public IObjectSet<VISITS_INPT_SUBUNIT> VISITS_INPT_SUBUNIT
        {
            get { return _VISITS_INPT_SUBUNIT; }
        }

        public IObjectSet<PT_MASTER> PT_MASTER
        {
            get { return _PT_MASTER; }
        }

        public IObjectSet<PT_ENCOUNTERS> PT_ENCOUNTERS
        {
            get { return _PT_ENCOUNTERS; }
        }

        public IObjectSet<ENCOUNTER_LINK> ENCOUNTER_LINK
        {
            get { return _ENCOUNTER_LINK; }
        }

        public IObjectSet<PT_SCAMP_STATUS> PT_SCAMP_STATUS
        {
            get { return _PT_SCAMP_STATUS; }
        }

        public IObjectSet<ADMIN_ENCOUNTER_STATUS> ADMIN_ENCOUNTER_STATUS
        {
            get { return _ADMIN_ENCOUNTER_STATUS; }
        }

        public IObjectSet<MEASUREMENT> MEASUREMENTS
        {
            get { return _MEASUREMENTS; }
        }

        public IObjectSet<ADMIN_SCREENING> ADMIN_SCREENING
        {
            get { return _ADMIN_SCREENING; }
        }

        public IObjectSet<ADMIN_SCAMPS_STATUS> ADMIN_SCAMPS_STATUS
        {
            get { return _ADMIN_SCAMPS_STATUS; }
        }

        public IObjectSet<PT_EN_LIST> PT_EN_LIST
        {
            get { return _PT_EN_LIST; }
        }

        public IObjectSet<EN_SUBUNITS_LIST> EN_SUBUNITS_LIST
        {
            get { return _EN_SUBUNITS_LIST; }
        }

        public IObjectSet<VW_ENCOUNTER_SEARCH> VW_ENCOUNTER_SEARCH
        {
            get { return _VW_ENCOUNTER_SEARCH; }
        }

        public IObjectSet<VW_PT_CURR_ENROLL> VW_PT_CURR_ENROLL
        {
            get { return _VW_PT_CURR_ENROLL; }
        }

        #endregion

        public int ExecuteStoreCommand(string commandText, params object[] parameters)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// emulates the view 'ENCOUNTER_OPTS_VIEW' which provides select options via a view.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cats"></param>
        /// <returns></returns>
        private IEnumerable<T> getOptList<T>(string[] cats)
        {

            var list = new List<ScampsDB.ViewModels.SelectOptionModel>();

            if(cats.Contains("0"))
                list.AddRange(
                        _SCAMPs.Where(sc => sc.VISIBLE != "N").Select(sc => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 0,
                            name = sc.SCAMP_NAME,
                            val = sc.SCAMP_ID.ToString()
                        })
                    );
            if (cats.Contains("1"))
                list.AddRange(
                        _ADMIN_PROVIDER.Where(pr => pr.PROFILE_NAME.Equals("DC",StringComparison.OrdinalIgnoreCase))
                        .Select(pr => new ScampsDB.ViewModels.SelectOptionModel(){
                            cat=1,name=pr.FULL_NAME, val=pr.SCAMPS_PROV_ID.ToString()
                        })
                    );
            if (cats.Contains("2"))
                list.AddRange(
                        _ADMIN_PROVIDER.Where(pr => !pr.PROFILE_NAME.Equals("DC", StringComparison.OrdinalIgnoreCase))
                        .Select(pr => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 2,
                            name = pr.FULL_NAME,
                            val = pr.SCAMPS_PROV_ID.ToString()
                        })
                    );
            if (cats.Contains("3"))
                list.AddRange(
                        _ADMIN_LOOKUP.Where(al => al.LOOKUP_CD.Equals("LOCTN_OUTPT",StringComparison.OrdinalIgnoreCase))
                        .Select(al => new ScampsDB.ViewModels.SelectOptionModel(){
                            cat=3, name=al.DESCRIPTION_TXT, val=al.VALUE_TXT
                        })
                    );
            if (cats.Contains("4"))
                list.AddRange(
                        _ADMIN_LOOKUP.Where(al => al.LOOKUP_CD.Equals("LOCTN_INPT", StringComparison.OrdinalIgnoreCase))
                        .Select(al => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 4,
                            name = al.DESCRIPTION_TXT,
                            val = al.VALUE_TXT
                        })
                    );
            if (cats.Contains("5"))
                list.AddRange(
                        _ADMIN_LOOKUP.Where(al => al.LOOKUP_CD.Equals("LOCTN_EVENT", StringComparison.OrdinalIgnoreCase))
                        .Select(al => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 5,
                            name = al.DESCRIPTION_TXT,
                            val = al.VALUE_TXT
                        })
                    );
            if (cats.Contains("6"))
                list.AddRange(
                        _ADMIN_LOOKUP.Where(al => al.LOOKUP_CD.Equals("EVENT_TYPE", StringComparison.OrdinalIgnoreCase))
                        .Select(al => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 6,
                            name = al.DESCRIPTION_TXT,
                            val = al.VALUE_TXT
                        })
                    );
            if (cats.Contains("7"))
                list.AddRange(
                        _ADMIN_ENCOUNTER_STATUS.Select(es => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 7,
                            name = es.STATUS_TEXT,
                            val = es.STATUS_ID.ToString()
                        })
                    );
            if (cats.Contains("8"))
                list.AddRange(
                        _ADMIN_ENCOUNTERS.Where(en => en.VISIBLE_FLAG == "Y" && en.WEB_FORM_APPROVED == "Y")
                        .Select(en => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 8,
                            name = en.ENCOUNTER_NAME,
                            val = en.ENCOUNTER_ID.ToString()
                        })
                    );
            if (cats.Contains("9"))
                list.AddRange(
                        _ADMIN_ENCOUNTERS.Where(en => en.VISIBLE_FLAG == "Y" && en.WEB_FORM_APPROVED == "Y")
                        .Select(en => new ScampsDB.ViewModels.SelectOptionModel()
                        {
                            cat = 9,
                            name = en.SCAMP_ID.ToString(),
                            val = en.ENCOUNTER_ID.ToString()
                        })
                    );

            return list as List<T>;
        }

        /// <summary>
        /// ExecuteStoreQuery is ran manually a few times by scampsEntity functions
        /// as a nasty hack to provide data to these queries, the command text is parsed to determine which dataset to return
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IEnumerable<T> ExecuteStoreQuery<T>(string commandText, params object[] parameters)
        {
            switch (typeof(T).ToString())
            {
                case "ScampsDB.ViewModels.SelectOptionModel":
                    var scrit = "cat in (";
		            var ecrit = ") order";
                    var sidx = commandText.IndexOf(scrit);
		            sidx += scrit.Length;
                    var eidx = commandText.IndexOf(ecrit, sidx);
                    var cats = commandText.Substring(sidx,eidx-sidx).Split(',');
                    return getOptList<T>(cats);
                case "ScampsDB.ViewModels.KeyValueModel":
                    switch (commandText)
                    {
                        case "select to_char(pt_encounters_appt_id_seq.nextval) as key, to_char(pt_encounters_har_seq.nextval) as value from dual":
                            var keyval = new ScampsDB.ViewModels.KeyValueModel[]{
                            new ScampsDB.ViewModels.KeyValueModel()
                            {
                                key = _PT_ENCOUNTERS.ID.ToString(),
                                value = HAR.ToString()
                            }};
                            return keyval as IEnumerable<T>;
                        case "select to_char(pt_encounters_appt_id_seq.nextval) as key, to_char(-1) as value from dual":
                            keyval = new ScampsDB.ViewModels.KeyValueModel[]{
                            new ScampsDB.ViewModels.KeyValueModel()
                            {
                                key = _PT_ENCOUNTERS.ID.ToString(),
                                value = "-1"
                            }};
                            return keyval as IEnumerable<T>;
                        default:
                            throw new NotImplementedException();
                    }
                default:
                    throw new NotImplementedException();
            }
            
        }

        public int SaveChanges()
        {
            return 1;
            //throw new NotImplementedException();
        }

        public void Refresh(RefreshMode mode, object Entity)
        {

            //throw new NotImplementedException();
        }
    }

}
