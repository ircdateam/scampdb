﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Serialization;

namespace ScampsDB.ViewModels
{
    /// <summary>
    /// ViewModels defined below define what columns/fields will be displayed from a query.
    /// view models can encompass composite queries (e.g. a record for pt_encounters resolving a provider's name or resolving the encounter/form info via pt_master or admin_encounters)
    /// By encompassing these expected fields with these view models, we can have a more coherent way of implementing their display and transport
    /// 
    /// further features that can be implemented for these classes are XML and JSON serialization (a TO-DO at this point)
    /// </summary>
    
    public static class ViewModelBaseExtensions
    {
        public static string ListToJSON<T>(this IEnumerable<T> l) where T : ViewModelBase
        {
            return JsonConvert.SerializeObject(l);
        }
        public static void StreamListToJSON(this IEnumerable<ViewModelBase> l, System.IO.TextWriter w)
        {
            using (var jwrite = new JsonTextWriter(w)){
                var jserial = JsonSerializer.Create();
                    jserial.Serialize(jwrite,l);
            }
        }
    }
    //ViewModelBase allows each view class to be easily serialized either to JSON or XML
    public abstract class ViewModelBase
    {

        /*
        public static string ListToJSON(IEnumerable<ViewModelBase> l)
        {
            return JsonConvert.SerializeObject(l);
        }*/
        public string ToJSON()
        {
            return JsonConvert.SerializeObject(this);
        }
        public string ToXML()
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(this.GetType());
            serializer.Serialize(stringwriter, this);
            return stringwriter.ToString();
        }
    }

    public class encounterData
    {
        public long apptid { get; set; }
        public string enstat { get; set; }
        public string encurl { get; set; }
        public decimal? fexist { get; set; }
        public string fname { get; set; }
        public string encd { get; set; }
        public int? subid { get; set; }
        public encounterData() { }
    }

    public class KeyValueModel : ViewModelBase
    {
        public string key { get; set; }
        public string value { get; set; }
        public KeyValueModel() { }
    }

    public class EncounterSubUnitModel : ViewModelBase
    {
        public encounterData edata { get; set; }
        public int? sub_id { get; set; }
        public long appt_id { get; set; }
        public string HAR { get; set; }
        public DateTime? encDate { get; set; }
        public string attending { get; set; }
        public string scampName { get; set; }
        public string encounterName { get; set; }
        public string statusDescription { get; set; }
    }

    public class PatientEncounterModel : ViewModelBase
    {
        public encounterData edata { get; set; }
        public string provider { get; set; }
        public string HAR { get; set; }
        public string visitType { get; set; }
        public string location { get; set; }
        public string scampName { get; set; }
        public string encounterName { get; set; }
        public string encounterStatus { get; set; }
        public string statusType { get; set; }
        public decimal? ageMinutes { get; set; }
        public DateTime? encounterDate { get; set; }
        public DateTime? dischargeDate { get; set; }

        public long? encounterLinkID { get; set; }
        public decimal? subunitID {get;set;}

        public PatientEncounterModel() { }

        /*
        public static string getJSONlist(IEnumerable<PatientEncounterModel> list)
        {
            return list.to
            return JsonConvert.SerializeObject(list);
        }*/
    }

    public class userProfileModel : ViewModelBase
    {
        public string user_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string full_name { get { return first_name + " " + last_name; }}
        public string title { get; set; }
        public int id { get; set; }
        public int scamps_prov_id { get; set; }
        public string email { get; set; }
        //null or 0 : active directory, 1 local db /w bcrypt
        public short? authmode { get; set; }
        public string hosp_prov_id { get; set; }
    }

    public class statData2D : ViewModelBase
    {
        public string[] labels {get; set;}
        public string[] datapoints {get;set;}
        public statData2D() { }
    }

    public class SelectOptionModel : ViewModelBase
    {
        public string name { get; set; }
        public string val { get; set; }
        public int cat { get; set; }

        public override string ToString()
        {
            return string.Concat(name, ",", val, ",", cat);
        }
    }

    public class SearchResultModel : ViewModelBase
    {

        public string pdata { get; set; }
        public encounterData edata { get; set; }
        public string mrn { get; set; }
        public string lname { get; set; }
        public string fname { get; set; }
        public DateTime? dob { get; set; }
        public string gender { get; set; }
        public string prov { get; set; }
        public string location { get; set; }
        public DateTime? date { get; set; }
        public string visit { get; set; }
        public string scamp { get; set; }
        public string encounter { get; set; }
        public string status { get; set; }

        public SearchResultModel() { }
    }

    public class SCAMPenrollModel : ViewModelBase
    {
        public string mrn { get; set; }
        public int scamp_id { get; set; }
        public string scamp_name { get; set; }
        public short? status_val { get; set; }
        public string cmnt_txt { get; set; }
        public string user_id { get; set; }
        public DateTime? audit_dt_tm { get; set; }
    }

    public class SCAMPenrollHistModel
    {
        public string mrn { get; set; }
        public string scamp_name { get; set; }
        public string user_id{ get; set; }
        public string old_val { get; set; }
        public string new_val { get; set; }
        public DateTime? audit_dt_tm { get; set; }
        public string comment { get; set; }
    }

    public class PatientViewModel : ViewModelBase
    {
        public string mrn {get;set;}
        public string lname {get;set;}
        public string fname {get;set;}
        public DateTime? dob { get;set;}
        public string gender {get;set;}
        public string comment { get; set; }

        public PatientViewModel() { }

    }

    public class EncounterUpdateModel : ViewModelBase
    {
        public DateTime? encounter_date { get; set; }
        public string appt_notes { get; set; }
        public int? scamps_prov_id { get; set; }
        public string loctn_abbr { get; set; }
    }
    
    public class EncounterViewModel : ViewModelBase
    {
        public DateTime? encounter_date { get; set; }
        public int? scamp_id { get; set; }
        public string appt_notes { get; set; }
        public int? scamps_prov_id { get; set; }
        public string loctn_abbr { get; set; }
        public int? encounter_type { get; set; }
        public short? encounter_status { get; set; }
        public DateTime? encounter_email { get; set; }
        public DateTime? encounter_prereview { get; set; }
        public DateTime? encounter_postreview { get; set; }
        public int? encounter_user { get; set; }
        public string encounter_notes { get; set; }
        public string event_type_abbr { get; set; }
        public short? prov_lock_status { get; set; }
        public DateTime? inpt_disch { get; set; }
        public long appt_id { get; set; }
        public int? sub_id { get; set; }
        public string mrn { get; set; }
        public string har { get; set; }
        public string encounter_cd { get; set; }
        public bool? hasData { get; set; }
    }

    public class EMRProviderModel : ViewModelBase
    {
        public int prov_id { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string initial { get; set; }
        public string suffix { get; set; }
        public string credentials { get; set; }
        public string email { get; set; }
        public int? emp_id { get; set; }
        public string cowname { get; set; }
        public bool active { get; set; }
    }

    public class EMRViewModel : ViewModelBase
    {
        static string[] theadings =
                {
                 "Hospital ID",
                 "First Name",
                 "Last Name",
                 "Middle Initial",
                 "Name Suffix",
                 "Credentials",
                 "Email Address",
                 "BCH Employee ID",
                 "Full Name (COW)",
                 "Provider Status"
                };

        public int hospitalID { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string initial { get; set; }
        public string suffix { get; set; }
        public string credentials { get; set; }
        public string email { get; set; }
        public int? EmployeeID { get; set; }
        public string EmployeeFlag { get; set; }
        public string COWname { get; set; }
        public string status { get; set; }

        public EMRViewModel()
        {
        }

        public override string ToString()
        {
            var para = new List<object>()
                           {
                               hospitalID,
                               fname,
                               lname,
                               initial,
                               suffix,
                               credentials,
                               email,
                               EmployeeID,
                               //EmployeeFlag,
                               COWname,
                               status
                           };
            var evm = new StringBuilder();
            int count = 0;
            foreach (Object ii in para)
            {

                if (ii is string && !string.IsNullOrEmpty((string)ii))
                    evm.Append(theadings[count] + ": " + ii + " ");
                if (ii is int? && ((int?)ii).HasValue)
                    evm.Append(theadings[count] + ": " + ii + " ");

                count++;
            }
            return evm.ToString();
        }

    }

    public class MeasurementViewModel
    {
        public string id { get; set; }
        public string ele_cd { get; set; }
        public string val { get; set; }
        public int seq { get; set; }

        public MeasurementViewModel() { }
        public MeasurementViewModel(string i, string e, string v, int s = 1)
        {
            id = i; ele_cd = e; val = v; seq = s;
        }
    }
}
